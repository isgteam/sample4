<?php
/* This page shows all the unassigned students in the DB. This will mostly comprises of any student who has not been manually assigned or has not been assigned by the automatic system. It basically makes 2 tables one for graduate and one for undergraduate  */

require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

if (($key = array_search('assignedClass', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('assignedInstructor', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('fixedClass', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('fixedInstructor', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}

if (!in_array('Year', $fieldsToDisplay)) {
    $fieldsToDisplay[] = 'Year';
}

$message = "";

if (isset($_POST['saveYear'])) {
    $year = $_POST['year'];
    $uid = $_POST['uid'];
    $newUid = $_POST['newUid'];
    $graduid = $_POST['graduid'];
    $newYear = $_POST['newYear'];
    $studentType = $_POST['studentType'];

    for ($i = 0; $i < count($uid); $i++) {

        if ($i < count($year) && $year[$i] != $newYear[$i]){

            $result = $database->update(
                "Grad",
                [
                    "Year" => trim($newYear[$i]),
                ],
                ["UID" => $graduid[$i]]
            );

            // Show if your update to the DB was successful or not

            if ($result->errorInfo()[2] == null) {
                $message = '<div class="alert alert-success" role="alert">
                    Your updates have been saved.  
                </div>';
            } else {
                $message = '<div class="alert alert-danger" role="alert">
                    An error occurred. Your updates were not saved.
                </div>';
            }
            getDBErrors($result);
        }

        if($newUid[$i] != $uid[$i]){
            $result = $database->update(
                $studentType[$i],
                [
                    "UID" => trim($newUid[$i])
                ],
                ["UID" => $uid[$i]]
            );

            // Show if your update to the DB was successful or not

            if ($result->errorInfo()[2] == null) {
                $message = '<div class="alert alert-success" role="alert">
                    Your updates have been saved.  
                </div>';
            } else {
                $message = '<div class="alert alert-danger" role="alert">
                    An error occurred. Your updates were not saved.
                </div>';
            }
            getDBErrors($result);
        }
    }

    $body .= $message;
}

$body .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';

$body .=  '
<div class="table-responsive">
<table id="dataTable" class="table table-bordered table-striped table-sm" >  
<thead>
    <tr>  
';

foreach ($fieldsToDisplay as $rec) {
    $body .= '<th scope="col">' . $tableDisplay[$rec] . '</th>';
}

$body .= '</tr>
</thead>
<tbody>';

$body .= makeTable("Ugrad") . makeTable("Grad");

$body .= '</tbody></table></div>';

$body .= '
    <input type="submit" name="saveYear" class="btn btn-primary" value="Save">
</form>';


$breadcrumb = array("Home" => "../", "Admin" => "index.php");

generatePage($body, "Administrative Portal-Unassigned Students", "Unassigned Students", $breadcrumb);

function makeTable($tableName)
{
    global $database;
    global $fieldsToDisplay;

    $body = "";

    $cols = array();

    // Check if graduate table has that col
    if (strcmp($tableName, "Grad") == 0) {
        foreach ($fieldsToDisplay as $fields) {
            if (in_array($fields, GRAD_TABLE)) {
                $cols[] = $fields;
            }
        }
    }
    // Check if graduate table has that col
    if (strcmp($tableName, "Ugrad") == 0) {
        foreach ($fieldsToDisplay as $fields) {
            if (in_array($fields, UGRAD_TABLE)) {
                $cols[] = $fields;
            }
        }
    }

    // Third element in the display field should always be the UID as that is the key in the DB.
    if (!in_array("UID", $cols)) {
        array_splice($cols, 2, 0, "UID");
    }

    $result = $database->select($tableName, $cols, 
    [
        "AND" => [
            "assignedClass[=]" => null,
            "OR" => [
                "fixedClass[=]" => "", 
                "fixedClass" => null
            ]
        ]
    ]);

    // If no one is unassigned then just return empty string
    if (count($result) == 0) {
        return '';
    }

    foreach ($result as $row) {
        $body .= '<tr>';
        foreach ($fieldsToDisplay as $key) {
            if (!in_array($key, $cols)) {
                $body .= "<td></td>";
            } else {
                $value = $row[$key];
                if (!$value) {
                    $value = "NONE";
                }
                // If you have to display transcript display a link
                if (strcmp("Transcript", $key) == 0 && strcmp($value, 'NONE') != 0) {
                    $value = 
                    '<a href="TranscriptPdf.php?UID=' . $row['UID'] . '&type=' . $tableName . '">Transcript</a>';
                } elseif (strcmp("Year", $key) == 0) {
                    $value = '<input type="text" name="newYear[]" value="' . $row['Year'] . '">
                    <input type="text" name="year[]" value="' . $row['Year'] . '" hidden>
                    <input type="text" name="graduid[]" value="' . $row['UID'] . '" hidden>';
                } elseif (strcmp("UID", $key) == 0) {
                    $value = '<input type="text" name="uid[]" value="' . $row['UID'] . '" hidden>
                    <input type="text" name="newUid[]" value="' . $row['UID'] . '">
                    <input type="text" name="studentType[]" value="' . $tableName . '" hidden>';
                }

                $body .= "<td>" . $value . "</td>";
            }
        }
    }

    return $body;
}
