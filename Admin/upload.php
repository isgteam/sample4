<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/support.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/AccessDB/DBquery.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

// Creating an associative array for links in breadcrumbs
$breadcrumb = array("Home" => "../", "Admin" => ".");

$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

$body = "";
$dataToInsert = array();

if (isset($_POST['SubmitButton'])) { //check if form was submitted

    if (isset($_FILES['filepath']['name']) && in_array($_FILES['filepath']['type'], $file_mimes)) {

        $arr_file = explode('.', $_FILES['filepath']['name']);
        $extension = end($arr_file);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($_FILES['filepath']['tmp_name']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        $worksheet = $spreadsheet->getActiveSheet();

        $body .= '<table id="dataTable" class="table table-striped table-bordered table-sm"><thead>';

        $i = 0;
        foreach ($worksheet->getRowIterator() as $row) {
            $body .= '<tr>';
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // This loops through all cells,
            //    even if a cell value is not set. By default, only cells that have a value set will be iterated.

            $values = [];
            $j=0;
            foreach ($cellIterator as $cell) {
                //For graduates add a GPA of 0 in the values array 
                if (strcmp($_POST['table'],"Grad") == 0 && $j == 4 && $i != 0)
                    $gottenValue = "0";
                else
                    $gottenValue = $cell->getValue();
                $body .= '<td>' .
                    $gottenValue.
                    '</td>';
                array_push($values, $gottenValue);
                $j++;
            }
            $body .= '</tr>';
            $i++;
            if ($i != 1) {

                $dataToInsert = array(
                    "FirstName" => $values[0],
                    "LastName" => $values[1],
                    "Phone" => "999-999-9999",
                    "Email" => $values[2],
                    "GPA" =>  $values[4],
                    "Semesters"=> $values[5],
                    "Department" => "CS"
                );                

                // regular expression matching of the courses because it is very arbitrary 
                $choice1 = array();
                $choice2 = array();
                $choice3 = array();

                preg_match('/[cC][mM][sS][cC]\s*([0-9]{3})|([0-9]{3})/', $values[6], $choice1);
                preg_match('/[cC][mM][sS][cC]\s*([0-9]{3})|([0-9]{3})/', $values[7], $choice2);
                preg_match('/[cC][mM][sS][cC]\s*([0-9]{3})|([0-9]{3})/', $values[8], $choice3);

                if( count($choice1) >= 2){
                    $dataToInsert += array("Choice_1" => "CMSC".$choice1[count($choice1)-1] );
                }
                if( count($choice2) >= 2){
                    $dataToInsert += array("Choice_2" => "CMSC".$choice2[count($choice2)-1] );
                }
                if( count($choice3) >= 2){
                    $dataToInsert += array("Choice_3" => "CMSC".$choice3[count($choice3)-1] );
                }

                if (count($values) >= 10)
                    $dataToInsert += array("Comments" => "Already TAed Courses ".$values[9]);

                $somethingWrong = true;

                if( $database->has($_POST['table'], ["UID[=]" => $values[3]])) {
                    $somethingWrong = false;
                    $temp = $database->update( $_POST['table'], $dataToInsert, ["UID[=]" => $values[3]] );
                    getDBErrors($temp);
                }
                else {
                    $dataToInsert += ["UID" => $values[3] ];
                    $data = $database->insert( $_POST['table'], $dataToInsert);
                    $somethingWrong = $data->rowCount() == 0;
                }

                if ($somethingWrong) {
                    echo "something went wrong for UID: ".$values[3]."<br>";
                    $i--;
                }

            }
            else {
                $body .= "</thead><tbody>";
            }
        }
        $body .= '</tbody></table>';
        $body = '<div class="alert alert-success" role="alert">
            File has been uploaded. '.$i.' Entries have been added/updated.
        </div>'.$body;
    }
} else
    $body = <<<HTML
        <form action="" method="post" class="text-center p-3" enctype="multipart/form-data">
            <input type="file" name="filepath" id="filepath" required="true"/>
            <select name="table" required="true">
                <option value="Grad">Graduate</option>
                <option value="Ugrad">Undergraduate</option>
            </select>
            <input type="submit" name="SubmitButton"/>
            <br>
            <br>
            <a href="/Assets/ExampleUploadFile.xlsx" download>Click here to download an example excel file.</a>
        </form>

HTML;

generatePage($body, "Administrative Portal", "Administrative Options", $breadcrumb = $breadcrumb);
