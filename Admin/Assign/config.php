<?php

$file = "list.json";

$json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Admin/Assign/' . $file), true);


function studentToHours($course)
{
    $courseSetting = getCourseLevelSetting($course);
    return ceil(($course['RegisteredStudents'] / $courseSetting['studentsEnrollmentRequirement'])
     * $courseSetting['TAHourAllocation']);
}


function NumberDisSectionToHours($course)
{
    $courseSetting = getCourseLevelSetting($course);
    return $course['NumSections'] * $courseSetting['DiscussionSecBonusHours'];
}

function getCourseLevelSetting($course) {
    global $json;
    
    foreach ($json['courseLevel'] as $courseLevels) {
        if ("CMSC".substr($courseLevels['name'], 0, 1) === substr($course['Name'], 0, 5)) {
            return $courseLevels;
        }
    }
    return array();
}


function doYouGetGrad($course) {
    $courseSetting = getCourseLevelSetting($course);
    return ceil($course["RegisteredStudents"] >= $courseSetting['studentsEnrollmentRequirement']);
}

// Rule 4​: Undergrad TAs can be hired to work 10 hours per week.Each grad TA is expected to work 20 hours per week
$hoursPerUgradTA = $json['UgradTAHours'];
$hoursPerGradTA =  $json['GradTAHours'];


// As a rule, no undergrad should TA more than one course, but there can be occasional approved exceptions.

// Can we done by manually assigning class

// Rule 5​: The ratio of grad to undergrad TAs should be no larger than 1 to 3 (i.e., 1 grad TA for every 3 or more undergrad TAs).
// These 2 variables contain the ratio of graduates vs number of undergraduates, that is the ratio which is set to 1/3 (Graduates to Undergraduates)
$numberOfGrads = $json['GradRatio'];
$numberOfUGrads = $json['UgradRatio'];


function calculateGrads($course)
{
    global $json, $hoursPerUgradTA, $hoursPerGradTA;
    $Ugrads = 0;
    $Grads = 0;
    $hours  = $course['Hours'] + $course['Additional_Hours'];

    if($hours < $hoursPerGradTA) {
        return 0;
    }

    while (($Ugrads * $hoursPerUgradTA + $Grads * $hoursPerGradTA) < $hours) {
        if($Grads == 0)
            $Grads ++;
        else if ($Ugrads == 0)
            $Ugrads ++;
        else if($Grads / $Ugrads  <=  $json['GradRatio'] / $json['UgradRatio'])
            $Grads += $json['GradRatio'];
        else
            $Ugrads += 1;
    }
    return $Grads;
}

function getNumberOfGrads($numUgrads)
{
    global $json;
    $Ugrads = 0;
    $Grads = 0;
    while ($Ugrads < $numUgrads) {
        $Ugrads += $json['UgradRatio'];
        $Grads += $json['GradRatio'];
    }
    return $Grads;
}

function calculateUgrads($course)
{
    global $json, $hoursPerUgradTA, $hoursPerGradTA;
    $Ugrads = 0;
    $Grads = 0;
    $hours  = $course['Hours'] + $course['Additional_Hours'];

    if($hours < $hoursPerGradTA) {
        while (($Ugrads+1) * $hoursPerUgradTA < $hours) {
            $Ugrads ++;
        }
        return $Ugrads;
    }

    while (($Ugrads * $hoursPerUgradTA + $Grads * $hoursPerGradTA) < $hours) {
        if($Grads == 0)
            $Grads ++;
        else if ($Ugrads == 0)
            $Ugrads ++;
        else if($Grads / $Ugrads  <=  $json['GradRatio'] / $json['UgradRatio'])
            $Grads += $json['GradRatio'];
        else
            $Ugrads += 1;
    }
    return $Ugrads;
}

// For graduate student

$StudentsinGrad = $json['StudentsinGrad'];
$GradTAinGrad = $json['GradTAinGrad'];

function getCourseHours($course) {
    $hours = NumberDisSectionToHours($course) + studentToHours($course);
    return $hours;
}

function isGradCourse($courseName)
{
    if (strcmp($courseName, "CMSC5") > 0) {
        return true;
    } else {
        return false;
    }
}

function isRatioRespected($course)
{
    global $database;
    $grads = $database->count(
        "Grad",
        [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 0
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 0
                ]
            ]
        ]
    ) + 
    $database->count(
        "Grad",
        [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 1
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 1
                ]
            ]
        ]
    );
    $ugrads = $database->count(
        "Ugrad",
        [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 0
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 0
                ]
            ]
        ]
    ) +
    $database->count(
        "Ugrad",
        [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 1
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME" => 1
                ]
            ]
        ]
    );

    return $ugrads <= calculateUgrads($course) && $grads <= calculateGrads($course);
}
