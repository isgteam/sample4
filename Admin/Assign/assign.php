<?php
/* This page shows all the assigned students in the DB.
This will mostly comprise of any student who has been manually assigned or has
been assigned by the automatic system. It basically makes 2 tables one for graduate
and one for undergraduate
*/
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once("config.php");

use Medoo\Medoo;

$courses = $database->select("Courses", "*", ["ORDER" => ["Name" => "DESC"]]);

$choiceUgrads = null;
$choiceGrads = null;

$failed = "";
$passed = "";

// Clean assignedClasses
$database->update("Grad", ["assignedClass" => null, "assignedInstructor" => null], Medoo::raw('WHERE TRUE'));
$database->update("Ugrad", ["assignedClass" => null, "assignedInstructor" => null], Medoo::raw('WHERE TRUE'));

$courseName = "";

foreach ($courses as $course) {
    $courseName = $course['Name'];

    // If the course is a graduate course
    if (isGradCourse($course['Name'])) {
        graduateClassAssignment($course);
    } else {
        undergraduateClassAssignment($course);
    }

    // Show the corresponding message if the task was successful or not
    $temp = $database->has("Grad", ["assignedClass[=]" => $course["Name"]])
        ||  $database->has("Ugrad", ["assignedClass[=]" => $course["Name"]]);
    if ($temp) {
        $passed .= $course["Name"] . '-' . $course['Instructor'] . ' ';
    } else {
        $failed .= $course["Name"] . '-' . $course['Instructor'] . ' ';
    }
}

echo "<script type='text/javascript'> 
        var answer = confirm('Automatic Assignment has completed. Successful: " . $passed . "it and failed: " . $failed . ". Do you want to go to Assigned Students page to look the assignments? '); 
        if ( answer )  { 
            window.location = '../assignedStudents.php'; 
        } else { 
            window.location = '../index.php'; 
        }   
        </script>";

function graduateClassAssignment($course)
{
    global $GradTAinGrad, $StudentsinGrad, $database, $choiceGrads;

    if ($course['RegisteredStudents'] >= $StudentsinGrad / $GradTAinGrad) {
        $noOfGradsNeeded = $course['RegisteredStudents'] / ($StudentsinGrad / $GradTAinGrad);
        $noOfGradsNeeded -= ($database->count(
            "Grad",
            [
                "fixedClass" => $course["Name"],
                "fixedInstructor" => $course["Instructor"],
                "HALF_TIME" => 0
            ]
        ) + 
        $database->count(
            "Grad",
            [
                "fixedClass" => $course["Name"],
                "fixedInstructor" => $course["Instructor"],
                "HALF_TIME" => 1
            ]
        )/2 );

        $choices = 1;
        $choiceGrads = array();
        while ((sizeof($choiceGrads) <= $noOfGradsNeeded) && $choices < 6) {
            // Now I also take a look at the number of graduates whose choice was
            $grads = $database->select(
                "Grad",
                [
                    "UID", "GPA", "Semesters", "gradStatus", "Year", "course1TAed", "Course1TAedOverlap", "course2TAed",
                    "Course2TAedOverlap", "course3TAed", "Course3TAedOverlap", "Choice_1", "Choice_2", "Choice_3",
                    "Choice_4", "Choice_5"
                ],
                [
                    "AND" => [
                        "Choice_" . $choices . "[=]" => $course["Name"],
                        "OR" => [
                            "fixedClass[=]" => "", 
                            "fixedClass" => null
                        ]
                    ],
                    "ORDER" => ["GPA" => "DESC"]
                ]
            );

            $choiceGrads = array_merge($choiceGrads, $grads);

            $choices += 1;
        }
        // sort the graduates
        usort($choiceGrads, "cmpGrads");

        // This line removes any extra Grads from the end of the array to match exactly 
        // the number of Graduate TAs needed
        $choiceGrads = array_slice($choiceGrads, 0, $noOfGradsNeeded);


        // This is where the changes to the DB happen in the Grad table. The Graduate Students are assigned the class.
        foreach ($choiceGrads as $student) {
            $temp = $database->update(
                "Grad",
                [
                    "assignedClass" => $course["Name"],
                    "assignedInstructor" => $course["Instructor"],
                    "Date_Assigned" => date("Y-m-d H:i:s")
                ],
                [
                    "UID[=]" => $student["UID"]
                ]
            );
            getDBErrors($temp);
        }
    }
}

function undergraduateClassAssignment($course)
{
    global $database, $choiceUgrads, $choiceGrads;

    // We need to calculate how many of undergraduate TA and graduate TA do we actually need
    $noOfUgradsNeeded = 0;
    $noOfGradsNeeded = 0;

    // This will be the case when the course we have is a 100-300 level
    if (doYouGetGrad($course)) {
        $noOfGradsNeeded = calculateGrads($course);
    }
    $noOfUgradsNeeded = calculateUgrads($course);


    // Decrease the # of grads and ugrads needed if some are already assigned in the system.
    $noOfUgradsNeeded -= ($database->count(
        "Ugrad",
        [
            "fixedClass" => $course["Name"],
            "fixedInstructor" => $course["Instructor"],
            "HALF_TIME" => 0
        ]
    ) 
    // Half Time assigned are half TAs
    + $database->count(
        "Ugrad",
        [
            "fixedClass" => $course["Name"],
            "fixedInstructor" => $course["Instructor"],
            "HALF_TIME" => 1
        ]
    )/2);
    $noOfGradsNeeded -= ($database->count(
        "Grad",
        [
            "fixedClass" => $course["Name"],
            "fixedInstructor" => $course["Instructor"],
            "HALF_TIME" => 0
        ]
    ) + 
    // Half Time assigned are half TAs
    $database->count(
        "Grad",
        [
            "fixedClass" => $course["Name"],
            "fixedInstructor" => $course["Instructor"],
            "HALF_TIME" => 1
        ]
    )/2 );

    // First I take a look at Choices and see if I can assign that Undergraduates to the class
    // who really want to be in it and if not enough then I take a look at the Choice 2 people and 
    // so on till I have enough people
    $choices = 1;
    $choiceUgrads = array();
    while ((sizeof($choiceUgrads) <= $noOfUgradsNeeded) && $choices < 6) {
        $ugrads = $database->select(
            "Ugrad",
            [
                "UID", "GPA", "Semesters", "course1TAed", "course2TAed", "course3TAed", "Choice_1", "Choice_1Grade",
                "Choice_2", "Choice_2Grade", "Choice_3", "Choice_3Grade", "Choice_4", "Choice_4Grade", "Choice_5",
                "Choice_5Grade"
            ],
            [
                "AND" => [
                    "Choice_" . $choices . "[=]" => $course["Name"],
                    "OR" => [
                        "fixedClass[=]" => "", 
                        "fixedClass" => null
                    ]
                ],
                "ORDER" => ["GPA" => "DESC"]
            ]
        );

        $choiceUgrads = array_merge($choiceUgrads, $ugrads);

        $choices += 1;
    }
    // sort the undergraduates
    usort($choiceUgrads, "cmpUgrads");

    // This line removes any extra ugrads from the end of the array to match exactly the number of undergraduates needed
    $choiceUgrads = array_slice($choiceUgrads, 0, $noOfUgradsNeeded);

    $choices = 1;
    $choiceGrads = array();
    while ((sizeof($choiceGrads) <= $noOfGradsNeeded) && $choices < 6) {
        // Now I also take a look at the number of graduates whose choice was
        $grads = $database->select(
            "Grad",
            [
                "UID", "GPA", "Year", "Semesters", "gradStatus", "course1TAed", "Course1TAedOverlap", "course2TAed",
                "Course2TAedOverlap", "course3TAed", "Course3TAedOverlap", "Choice_1", "Choice_2", "Choice_3",
                "Choice_4", "Choice_5"
            ],
            [
                "AND" => [
                    "Choice_" . $choices . "[=]" => $course["Name"],
                    "OR" => [
                        "fixedClass[=]" => "", 
                        "fixedClass" => null
                    ]
                ],
                "ORDER" => ["GPA" => "DESC"]
            ]
        );

        $choiceGrads = array_merge($choiceGrads, $grads);

        $choices += 1;
    }
    // sort the graduates
    usort($choiceGrads, "cmpGrads");

    // This line removes any extra Grads from the end of the array to match exactly the number of Graduate TAs needed
    $choiceGrads = array_slice($choiceGrads, 0, $noOfGradsNeeded);

    // This line maintains the ratio
    $choiceGrads = array_slice($choiceGrads, 0, getNumberOfGrads(count($choiceUgrads)));

    // This is where the changes to the DB happen in the Ugrad table. The students are assigned a class
    foreach ($choiceUgrads as $student) {
        $temp = $database->update(
            "Ugrad",
            [
                "assignedClass" => $course["Name"],
                "assignedInstructor" => $course["Instructor"],
                "Date_Assigned" => date("Y-m-d H:i:s")
            ],
            [
                "UID[=]" => $student["UID"]
            ]
        );
        getDBErrors($temp);
    }


    // This is where the changes to the DB happen in the Grad table. The Graduate Students are assigned the class.
    foreach ($choiceGrads as $student) {
        $temp = $database->update(
            "Grad",
            [
                "assignedClass" => $course["Name"],
                "assignedInstructor" => $course["Instructor"],
                "Date_Assigned" => date("Y-m-d H:i:s")
            ],
            [
                "UID[=]" => $student["UID"]
            ]
        );
        getDBErrors($temp);
    }
}

// These compare functions sorts in descending order of score
function cmpUgrads($a, $b)
{
    $aScore = $a['GPA'] / 4 * 10;
    $bScore = $b['GPA'] / 4 * 10;

    $aScore += $a['Semesters'] / 12 * 10;
    $bScore += $b['Semesters'] / 12 * 10;

    $aScore += choiceScore($a) / 5 * 10;
    $bScore += choiceScore($b) / 5 * 10;

    $aScore += gradeScore($a) / 9 * 10;
    $bScore += gradeScore($b) / 9 * 10;

    $aScore += previousTAScore($a) / 3 * 10;
    $bScore += previousTAScore($b) / 3 * 10;

    if ($aScore == $bScore) {
        return 0;
    }
    return ($aScore > $bScore) ? -1 : 1;
}

function cmpGrads($a, $b)
{
    $aScore = $a['GPA'] / 4 * 10;
    $bScore = $b['GPA'] / 4 * 10;

    $aScore += $a['Semesters'] / 12 * 10;
    $bScore += $b['Semesters'] / 12 * 10;

    $aScore += choiceScore($a) / 5 * 10;
    $bScore += choiceScore($b) / 5 * 10;

    $aScore += overlapScore($a) / 2 * 2;
    $bScore += overlapScore($b) / 2 * 2;

    if (strcmp($a["gradStatus"], "PHD") && date("Y") - $a['Year'] <= 2) {
        $aScore += 10;
    }

    if (strcmp($b["gradStatus"], "PHD") && date("Y") - $b['Year'] <= 2) {
        $bScore += 10;
    }

    if ($aScore == $bScore) {
        return 0;
    }
    return ($aScore > $bScore) ? -1 : 1;
}

function gradeComparator($grade)
{
    if (strcmp($grade, "A+") == 0) {
        return 9;
    }
    if (strcmp($grade, "A") == 0) {
        return 8;
    }
    if (strcmp($grade, "A-") == 0) {
        return 7;
    }
    if (strcmp($grade, "B+") == 0) {
        return 6;
    }
    if (strcmp($grade, "B") == 0) {
        return 5;
    }
    if (strcmp($grade, "B-") == 0) {
        return 4;
    }
    if (strcmp($grade, "C+") == 0) {
        return 3;
    }
    if (strcmp($grade, "C") == 0) {
        return 2;
    }
    if (strcmp($grade, "C-") == 0) {
        return 1;
    }
    return 0;
}

function choiceScore($a)
{
    global $courseName;

    if (strcmp($a['Choice_1'], $courseName) == 0) {
        return  5;
    } elseif (strcmp($a['Choice_2'], $courseName) == 0) {
        return 4;
    } elseif (strcmp($a['Choice_3'], $courseName) == 0) {
        return 3;
    } elseif (strcmp($a['Choice_4'], $courseName) == 0) {
        return 2;
    } elseif (strcmp($a['Choice_5'], $courseName) == 0) {
        return 1;
    }

    return  0;
}

function gradeScore($a)
{
    global $courseName;

    if (strcmp($a['Choice_1'], $courseName) == 0) {
        return gradeComparator($a['Choice_1Grade']);
    } elseif (strcmp($a['Choice_2'], $courseName) == 0) {
        return gradeComparator($a['Choice_2Grade']);
    } elseif (strcmp($a['Choice_3'], $courseName) == 0) {
        return gradeComparator($a['Choice_3Grade']);
    } elseif (strcmp($a['Choice_4'], $courseName) == 0) {
        return gradeComparator($a['Choice_4Grade']);
    } elseif (strcmp($a['Choice_5'], $courseName) == 0) {
        return gradeComparator($a['Choice_5Grade']);
    }

    return 0;
}

function overlapScore($a)
{
    global $courseName;

    if (strcmp($a['course1TAed'], $courseName) == 0) {
        return strcmp($a['Course1TAedOverlap'], "YES") ? 1 : 2;
    } elseif (strcmp($a['course2TAed'], $courseName) == 0) {
        return strcmp($a['Course2TAedOverlap'], "YES") ? 1 : 2;
    } elseif (strcmp($a['course3TAed'], $courseName) == 0) {
        return strcmp($a['Course3TAedOverlap'], "YES") ? 1 : 2;
    }

    return 0;
}

function previousTAScore($a)
{
    global $courseName;

    if (strcmp($a['course1TAed'], $courseName) == 0) {
        return 3;
    } elseif (strcmp($a['course2TAed'], $courseName) == 0) {
        return 2;
    } elseif (strcmp($a['course3TAed'], $courseName) == 0) {
        return 1;
    }

    return 0;
}
