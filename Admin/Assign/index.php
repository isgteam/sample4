<?php
/* This script is written for manually assigning the TA. The thought process was that there would be some people who will be recommended by the professor or some who have to have a position for them. This can be achieved through this script.

This script also relies on the DBquery file, the new support file present in the same folder and the filter option. */
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

$fieldsToDisplay[] = "HALF_TIME";
$fieldsToDisplay[] = "ADMIN_NOTES";

$ins = $database->select("Instructors", "*");

$message = "";

if (isset($_POST['addFixed'])) {
    $classes = $_POST['class'];
    $uids = $_POST['uid'];
    $studentType = $_POST['studentType'];
    $adminNotes = $_POST['adminNotes'];
    $instructor = $_POST['instructor'];
    $halfTime = $_POST['halfTime'];

    for ($i = 0; $i < count($_POST['class']); $i++) {
        if ($classes[$i] != "NONE") {
            $result = $database->update(
                $studentType[$i],
                [
                    "fixedClass" => trim($classes[$i]),
                    "fixedInstructor" => trim($instructor[$i]),
                    "Date_Assigned" => date("Y-m-d H:i:s"),
                    "HALF_TIME" => in_array($uids[$i], $halfTime) ? 1:0, 
                    "ADMIN_NOTES" => trim($adminNotes[$i]),
                ],
                ["UID" => $uids[$i]]
            );

            // Show if your update to the DB was successful or not

            if ($result->errorInfo()[2] == null) {
                $message = '<div class="alert alert-success" role="alert">
                    Your updates have been saved.  
                </div>';
            } else {
                $message = '<div class="alert alert-danger" role="alert">
                    An error occurred. Your updates were not saved.
                </div>';
            }
            getDBErrors($result);
        }
    }

    $body .= $message;
}

$body .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';

// Make Undergraduate table and Graduate table
$body .= '
<div class="table-responsive">
<table id="dataTable" class="table table-bordered table-striped table-sm" >  
<thead>
    <tr>';

foreach ($fieldsToDisplay as $record) {
    $body .= '<th scope="col">' . $tableDisplay[$record] . '</th>';
}
$body .= '
    </tr>
</thead>
<tbody>';

$body .= makeTable("Ugrad") . makeTable("Grad");

$body .= '</tbody></table></div>';

$body .= '
    <input type="submit" name="addFixed" class="btn btn-primary" formaction="index.php" formmethod="post" value="Save">
</form>';

$breadcrumb = array("Home" => "../../", "Admin" => "../", "Assign" => "index.php");

generatePage($body, "Administrative Portal-All Students", "All Applicants", $breadcrumb);

function makeTable($tableName)
{
    global $fieldsToDisplay, $ins;
    global $database;

    $table = "";

    $cols = array();

    // Check if graduate table has that col
    if (strcmp($tableName, "Grad") == 0) {
        foreach ($fieldsToDisplay as $fields) {
            if (in_array($fields, GRAD_TABLE)) {
                $cols[] = $fields;
            }
        }
    }
    // Check if graduate table has that col
    if (strcmp($tableName, "Ugrad") == 0) {
        foreach ($fieldsToDisplay as $fields) {
            if (in_array($fields, UGRAD_TABLE)) {
                $cols[] = $fields;
            }
        }
    }

    $result = $database->select($tableName, $cols);

    foreach ($result as $row) {
        $table .= '<tr>';
        foreach ($fieldsToDisplay as $key) {
            if (!in_array($key, $cols)) {
                $table .= "<td></td>";
            } else {
                $value = $row[$key];

                // If there is no value that the DB had null in that column
                if (!$value) {
                    $value = " ";
                }

                // If you have to display transcript display a link
                if (strcmp("Transcript", $key) == 0 && strcmp($value, " ") != 0) {
                    $value = '<a href="../TranscriptPdf.php?UID=' . $row['UID'] . '&type=' . $tableName . '">Transcript</a>';
                } elseif (strcmp("fixedClass", $key) == 0) {
                    $row['fixedClass'] = $value;
                    $value = '<input type="text" name="class[]" value="' . $row['fixedClass'] . '">
                          <input type="text" name="uid[]" value="' . $row['UID'] . '" hidden>
                          <input type="text" name="studentType[]" value="' . $tableName . '" hidden>';
                } elseif (strcmp("fixedInstructor", $key) == 0) {
                    $row['fixedInstructor'] = $value;
                    $instructors = '<select name="instructor[]"><option value=""> </option>';

                    foreach($ins as $i)
                        $instructors .= '<option value="'.$i["Name"].'" '.(strcmp($i["Name"], $value) == 0 ? "selected": "").' >'.$i["Name"].'</option>';

                    $instructors .= "</select>";
                    $value = $instructors;
                } elseif (strcmp("HALF_TIME", $key) == 0) {
                    $row['HALF_TIME'] = $value;
                    $value = '<input type="checkbox" name="halfTime[]" value="'.$row['UID'].'" '.($row["HALF_TIME"] == 0 ? "": "checked").'>';
                } elseif (strcmp("ADMIN_NOTES", $key) == 0) {
                    $row['ADMIN_NOTES'] = $value;
                    $value = '<input type="text" name="adminNotes[]" value="' . $row['ADMIN_NOTES'] . '">';
                }

                $table .= "<td>" . $value . "</td>";
            }
        }
        $table .= '</tr>';
    }

    return $table;
}
