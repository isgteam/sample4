<?php
// This is the settings file in which you can set the rule parameters which are taken in account
// when doing automatic assignment
require_once($_SERVER['DOCUMENT_ROOT'].'/support.php');

$file = "list.json";
$json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Admin/Assign/'.$file), true);
$body = "";

if (isset($_POST['saveConfig'])) {
    $json['GradTAHours'] = $_POST['GradTAHours'];
    $json['UgradTAHours'] = $_POST['UgradTAHours'];
    $json['GradRatio'] = $_POST['GradRatio'];
    $json['UgradRatio'] = $_POST['UgradRatio'];
    $json['StudentsinGrad'] = $_POST['StudentsinGrad'];
    $json['GradTAinGrad'] = $_POST['GradTAinGrad'];
    $i=0;
    $temp = array();
    foreach ($json['courseLevel'] as $class) {
        $class['name'] = $_POST['class'][$i];
        $class['studentsEnrollmentRequirement'] = (int)$_POST['studentsEnrollmentRequirement'][$i];
        $class['TAHourAllocation'] = (int)$_POST['TAHourAllocation'][$i];
        $class['DiscussionSecBonusHours'] = (int)$_POST['DiscussionSecBonusHours'][$i];
        array_push($temp, $class);
        $i++;
    }
    $json['courseLevel'] = $temp;

    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/Admin/Assign/'.$file, json_encode($json));

    $body .= '<div class="alert alert-success" role="alert">
        Configuration Saved. 
    </div>';
}

$GradTAHours = $json['GradTAHours'];
$UgradTAHours = $json['UgradTAHours'];

$GradRatio = $json['GradRatio'];
$UgradRatio = $json['UgradRatio'];

$StudentsinGrad = $json['StudentsinGrad'];
$GradTAinGrad = $json['GradTAinGrad'];

$courseLevel = $json['courseLevel'];

$body .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';

// Make Undergraduate table and Graduate table

$body .= '
<br>
<h2>Class policies</h2>
    <ul class="list-group list-group-flush text-left">
        <li class="list-group-item"><b>Rule 1​:</b> <br>
            <div class="table-responsive">
                <table class="table table-bordered table-striped" >  
                    <thead>
                        <tr>
                            <th scope="col">Course Level</th>
                            <th scope="col">Student Enrollment Requirement</th>
                            <th scope="col">TA Hour allocation</th>
                            <th scope="col">Hour Bonus/# discussion sections</th>
                        </tr>
                    </thead>
                    <tbody>';
foreach ($courseLevel as $class) {
    $body .= '
    <tr>
        <td><input type="text" name="class[]" value="'.$class['name'].'"></td>
        <td><input type="text" name="studentsEnrollmentRequirement[]" 
        value="'.$class['studentsEnrollmentRequirement'].'"></td>
        <td><input type="text" name="TAHourAllocation[]" value="'.$class['TAHourAllocation'].'"></td>
        <td><input type="text" name="DiscussionSecBonusHours[]" value="'.$class['DiscussionSecBonusHours'].'"></td>
    </tr>';
}

$body .= '      </tbody>
            </table>
        </div>
    </li>
    <li class="list-group-item"><b>Rule 2:</b> 
        Each grad TA is expected to work 
        <input type="text" name="GradTAHours" value="' . $GradTAHours . '"> hours per week
    </li>
    <li class="list-group-item"><b>Rule 3​:</b> 
        Undergrad TAs can be hired to work 
        <input type="text" name="UgradTAHours" value="' . $UgradTAHours . '"> 
        hours per week. As a rule, no undergrad should TA more than one course.
    </li>
    <li class="list-group-item">
        <b>Rule 4:</b> The ratio of grad to undergrad TAs should be no larger than 
        <input type="text" name="GradRatio" value="' . $GradRatio . '"> to 
        <input type="text" name="UgradRatio" value="' . $UgradRatio . '">.
    </li>
    <li class="list-group-item">
    <b>Rule 5:</b> For every 
    <input type="text" name="StudentsinGrad" value="' . $StudentsinGrad . '">
    students, a Grad-level class is allocated 
    <input type="text" name="GradTAinGrad" value="' . $GradTAinGrad . '"> Graduate TA.
    </li>
    </ul>
    <br>
    <input type="submit" name="saveConfig" class="btn btn-primary" formaction="setting.php" 
    formmethod="post" value="Save">
</form>';


$breadcrumb = array("Home" => "../../", "Admin" => "../");

generatePage($body, "Administrative Portal-All Students", "Setting for Auto Assign System", $breadcrumb);
