<?php
/* This page shows all the assigned students in the DB. This will mostly comprises of any student who has been manually assigned or has been assigned by the automatic system. It basically makes 2 tables one for graduate and one for undergraduate  */
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once('filter.php');

$breadcrumb = array("Home" => "../", "Admin" => "index.php");

$message = "";

if (isset($_POST['Remove'])) {
    $person = $_POST['RemovePerson'];
    $result = $database->update(
        $_POST['RemoveTable'],
        [
            "assignedClass" => null,
            "assignedInstructor" => null,
            "fixedClass" => null,
            "fixedInstructor" => null
        ],
        ["UID" => $person]
    );

    if($result != null){
        if ($result->errorInfo()[2] == null) {
            $message = '<div class="alert alert-success" role="alert">
                UID: ' . $person . ' was removed from class. 
            </div>';
        } else {
            $message = '<div class="alert alert-danger" role="alert">
                UID: ' . $person . ' could not be removed. An error occurred.
            </div>';
        }
        $body .= $message;
        getDBErrors($result);
    }
}

$courses = $database->select(
    "Courses",
    [
        'Name',
        'Instructor'
    ]
);

if (($key = array_search('assignedClass', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('assignedInstructor', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('fixedClass', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('fixedInstructor', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}

$fieldsToDisplay[] = "ADMIN_NOTES";

// Make header of the table
$body .= '
<div class="table-responsive">
<table id="dataTable" class="table table-bordered table-striped table-sm" >  
<thead>
    <tr>
        <th scope="col">Course</th>
        <th scope="col">Instructor</th>';

foreach ($fieldsToDisplay as $f) {
    $body .= '<th scope="col">' . $tableDisplay[$f] . '</th>';
}

$body .= '
        <th scope="col">Remove from Assigned Class</th>
    </tr>
</thead>
<tbody>';

foreach ($courses as $rec) {
    // Make Graduate table
    $body .= makeTable("Grad", $rec);
    // Make Undergraduate table
    $body .= makeTable("Ugrad", $rec);
}

$body .= '</tbody></table></div>';

generatePage($body, "Administrative Portal-Assigned Students", "Assigned Students", $breadcrumb);

function makeTable($tableName, $rec)
{
    global $fieldsToDisplay;
    global $database;

    $course = $rec['Name'];
    $ins = $rec['Instructor'];

    $table = "";

    $cols = array();

    // Check if graduate table has that col
    if (strcmp($tableName, "Grad") == 0) {
        foreach ($fieldsToDisplay as $fields) {
            if (in_array($fields, GRAD_TABLE)) {
                $cols[] = $fields;
            }
        }
    }
    // Check if graduate table has that col
    if (strcmp($tableName, "Ugrad") == 0) {
        foreach ($fieldsToDisplay as $fields) {
            if (in_array($fields, UGRAD_TABLE)) {
                $cols[] = $fields;
            }
        }
    }

    // Third element in the display field should always be the UID as that is the key in the DB.
    if (!in_array("UID", $cols)) {
        array_splice($cols, 2, 0, "UID");
    }

    $result = $database->select(
        $tableName,
        $cols,
        [
            "OR" => [
                "AND #key 1" => [
                    "assignedClass" => $course,
                    "assignedInstructor" => $ins,
                ],
                "AND #key 2" => [
                    "fixedClass" => $course,
                    "fixedInstructor" => $ins,
                ]
            ]
        ]
    );

    // If no one is assigned then just return empty string
    if (count($result) == 0) {
        return '';
    }

    foreach ($result as $row) {
        $table .= '<tr>';
        $table .= "<td>" . $course . "</td>";
        $table .= "<td>" . $ins . "</td>";
        foreach ($fieldsToDisplay as $key) {
            if (!in_array($key, $cols)) {
                $table .= "<td></td>";
            } else {
                $value = $row[$key];
                if (!$value) {
                    $value = "NONE";
                }
                // If you have to display transcript display a link
                if (strcmp("Transcript", $key) == 0) {
                    $table .= '<td><a href="TranscriptPdf.php?UID=' . $row['UID'] . '&type=' . $tableName . '">Transcript</a></td>';
                } else {
                    $table .= "<td>" . $value . "</td>";
                }
            }
        }
        $table .= '<td>
        <form action="' . $_SERVER['PHP_SELF'] . '" method="post" class="form-horizontal">
            <input type="submit" name="Remove" class="btn btn-danger" formaction="assignedStudents.php" formmethod="post" value="Remove">
            <input type="text" name="RemovePerson" value="' . $row["UID"] . '" hidden>
            <input type="text" name="RemoveTable" value="' . $tableName . '" hidden>
        </form>
        </td></tr>';
    }



    return $table;
}
