<?php
// This depends on UMD.IO open source project. http://github.com/umdio. I have asked CS IT to host this application. This is running on a docker server in the CS department called dvm01
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/Assign/config.php');

$link = 'http://zetsubo.cs.umd.edu:3000/v0';

use Medoo\Medoo;

$semesters = explode(",", file_get_contents($link.'/courses/semesters'));

$latestSemester = str_replace("]", "", $semesters[count($semesters) - 1]);

$courses = file_get_contents($link.'/courses?per_page=250&dept_id=CMSC&semester=' . $latestSemester);
$courses = json_decode($courses, true);
$instructors = array();

// Clear DB
$database->delete("Courses", Medoo::raw('WHERE TRUE'));

foreach ($courses as $r) {
    foreach ($r["sections"] as $section) {
        $sec = file_get_contents($link.'/courses/sections/' . $section . '?semester=' . $latestSemester);
        $sec = json_decode($sec, true);

        print("Fetching Data for: ");
        print($section);
        echo '<br>';

        //  Is this a discussion section
        $discussionSection = false;
        foreach ($sec["meetings"] as $meet) {
            $discussionSection = $discussionSection | strcmp($meet['classtype'], "Discussion") == 0;
        }

        $instructors = array_unique(array_merge($instructors, $sec['instructors']));

        $courseTemp  = ["Name" => $r['course_id'], "RegisteredStudents" => 0, "NumSections" => 0];

        // If DB already has the course and the instructor then update the Course
        if ($database->has("Courses", [
            "AND" => [
                "Name" => $r['course_id'],
                "Instructor" => implode(" , ", $sec['instructors'])
            ]
        ])) {
            $existingCourse = $database->select(
                "Courses",
                [
                    "RegisteredStudents",
                    "NumSections"
                ],
                [
                    "AND" => [
                        "Name" => $r['course_id'],
                        "Instructor" => implode(" , ", $sec['instructors'])
                    ]
                ]
            )[0];

            $courseTemp["RegisteredStudents"]  = $existingCourse["RegisteredStudents"] + ($sec['seats'] - $sec['open_seats']);
            if ($discussionSection) {
                $courseTemp["NumSections"] = $existingCourse["NumSections"] + 1;
            }

            $temp = $database->update(
                "Courses",
                [
                    "NumStudents[+]" =>  $sec['seats'],
                    "RegisteredStudents" => $courseTemp["RegisteredStudents"],
                    "NumSections" => $courseTemp["NumSections"],
                    "Hours" => getCourseHours($courseTemp)
                ],
                [
                    "AND" => [
                        "Name" => $r['course_id'],
                        "Instructor" => implode(" , ", $sec['instructors'])
                    ]
                ]
            );
            getDBErrors($temp);
        }
        // Insert the course in the table if it does not exists
        else {
            $courseTemp["RegisteredStudents"]  = ($sec['seats'] - $sec['open_seats']);
            if ($discussionSection) {
                $courseTemp["NumSections"] = 1;
            }

            $temp = $database->insert(
                "Courses",
                [
                    "NumStudents" => $sec['seats'],
                    "Instructor" => implode(" , ", $sec['instructors']),
                    "RegisteredStudents" => $courseTemp["RegisteredStudents"] ,
                    "Name" => $sec['course'],
                    "NumSections" => $courseTemp["NumSections"],
                    "Hours" => getCourseHours($courseTemp)
                ]
            );
            getDBErrors($temp);
        }
    }
}

// Adds instructors to the Table
foreach ($instructors as $ins) {
    if (!$database->has("Instructors", ["Name" => $ins])) {
        $database->insert("Instructors", [
            "Name" => $ins
        ]);
    }
}

echo "<script type='text/javascript'> var answer = confirm('Courses should have been updated on the Database.'); window.location = 'index.php';</script>";
