<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/Assign/config.php');

$body = "";
$message = "";

if (isset($_POST['updateCourse'])) {

    $courseNames = $_POST['courseName'];
    $instructorNames = $_POST['instructorName'];
    $addHours = $_POST['addHours'];
    $adminNotes = $_POST['adminNotes'];

    for ($i = 0; $i < count($courseNames); $i++) {

        $result = $database->update(
            "Courses",
            [
                "Additional_Hours" => $addHours[$i],
                "Admin_Notes" => $adminNotes[$i]
            ],
            [
                "AND #key 1" => [
                    "Name" => $courseNames[$i],
                    "Instructor" => $instructorNames[$i],
                ],
            ]
        );

        // Show if your update to the DB was successful or not
        if ($result->errorInfo()[2] == null) {
            $message = '<div class="alert alert-success" role="alert">
                Your updates have been saved.  
            </div>';
        } else {
            $message = '<div class="alert alert-danger" role="alert">
                An error occurred. Your updates were not saved.
            </div>';
        }
        getDBErrors($result);
    }
    $body .= $message;
}

if (isset($_POST['addCourse'])) {
    $result = $database->insert("Courses", [
        'Name' => trim($_POST["addCourseName"]),
        'Instructor' => trim($_POST["addInstructor"]),
        'NumSections' => trim($_POST["addDisSec"]),
        'NumStudents' => trim($_POST["addStudents"]),
        'RegisteredStudents' => trim($_POST["addStudents"]),
        'Hours' => trim($_POST["addHours"]),
        'Additional_Hours' => trim($_POST["addAdditionalHours"]),
        'Admin_Note' => trim($_POST["addNotes"])
    ]);

    // Show if your update to the DB was successful or not
    if ($result->errorInfo()[2] == null) {
        $message = '<div class="alert alert-success" role="alert">
                Course has been added.
            </div>';
    } else {
        $message = '<div class="alert alert-danger" role="alert">
                An error occurred. Course not added.
            </div>';
    }
    $body .= $message;
}

$courses = $database->select("Courses", "*");
$numberOfCourses = $database->count("Courses", "Name");
$numberOfInstructors = $database->count("Instructors");

$numberOfAssignedUgradHalf = $database->count("Ugrad", [
    "AND" => [
        "OR" => [
            "assignedClass[!]" => null,
            "OR" => [
                "fixedClass[!] #Key" => "", 
                "fixedClass[!]" => null
            ]
        ],
        "HALF_TIME[=]" => 1
    ]
]);



$numberOfAssignedUgradFull = $database->count("Ugrad", [
    "AND" => [
        "OR" => [
            "assignedClass[!]" => null,
            "OR" => [
                "fixedClass[!] #Key" => "", 
                "fixedClass[!]" => null
            ]
        ],
        "HALF_TIME[=]" => 0
    ]
]);

$numberOfAssignedGradHalf = $database->count("Grad", [
    "AND" => [
        "OR" => [
            "assignedClass[!]" => null,
            "OR" => [
                "fixedClass[!] #key" => "", 
                "fixedClass[!]" => null
            ]
        ],
        "HALF_TIME[=]" => 1
    ]
]);

$numberOfAssignedGradFull = $database->count("Grad", [
    "AND" => [
        "OR" => [
            "assignedClass[!]" => null,
            "OR" => [
                "fixedClass[!] #Key" => "", 
                "fixedClass[!]" => null
            ]
        ],
        "HALF_TIME[=]" => 0
    ]
]);

$hoursAssigned = $numberOfAssignedUgradFull * $hoursPerUgradTA + $numberOfAssignedGradFull * $hoursPerGradTA + $numberOfAssignedUgradHalf * $hoursPerUgradTA/2 + $numberOfAssignedGradHalf * $hoursPerGradTA/2;
$hoursLeft = $database->sum("Courses", "Hours") + $database->sum("Courses", "Additional_Hours") - $hoursAssigned;
$numberOfClassesWithHighRatio = 0;
$numberOfClassesWithHoursLeft = 0;

if (count($courses) != 0) {

    $table = "";

    foreach ($courses as $course) {
        $totalHours = $course["Hours"] + $course["Additional_Hours"];

        $ugradHours = $database->count("Ugrad", [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 0
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 0
                ]
            ]
        ]) * $hoursPerUgradTA +
        $database->count("Ugrad", [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 1
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 1
                ]
            ]
        ]) * $hoursPerUgradTA/2;

        $gradHours = $database->count("Grad", [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 0
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 0
                ]
            ]
        ]) * $hoursPerGradTA +
        $database->count("Grad", [
            "OR #a" => [
                "AND #key 1" => [
                    "assignedClass" => $course['Name'],
                    "assignedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 1
                ],
                "AND #key 2" => [
                    "fixedClass" => $course['Name'],
                    "fixedInstructor" => $course['Instructor'],
                    "HALF_TIME[=]" => 1
                ]
            ]
        ]) * $hoursPerGradTA/2;

        $table .= '<tr class="' . returnWarning($ugradHours != 0 && !isRatioRespected($course)) . '">
        <td>' . $course["Name"] . '<input type="text" name="courseName[]" value="' . $course["Name"] . '" hidden></td>
        <td>' . $course["NumSections"] . '</td>
        <td>' . $course["Instructor"] . '<input type="text" name="instructorName[]" value="' . $course["Instructor"] . '" hidden></td>
        <td>' . $course["RegisteredStudents"] . '</td>
        <td>' . $ugradHours . '</td>
        <td>' . $gradHours . '</td>
        <td>' . preg_replace('/^(\d+)$/', "+$1", (($totalHours - ($ugradHours + $gradHours)) * -1)) . '</td>
        <td>' . $totalHours . '</td>
        <td><input type="text" name="addHours[]" value="' . $course["Additional_Hours"] . '"></td>
        <td><input type="text" name="adminNotes[]" value="' . $course["Admin_Notes"] . '"></td>
        </tr>';

        if ($ugradHours != 0 && !isRatioRespected($course)) {
            $numberOfClassesWithHighRatio += 1;
        }
        if ($totalHours - ($ugradHours + $gradHours) > 0) {
            $numberOfClassesWithHoursLeft += 1;
        }
    }
    $body .=  <<<HTML
    <div class="card text-center" >
        <div class="card-body">
            <h4 class="card-title">Dashboard</h4>
            <table class="table card-text table-sm">
            <tr>
                <td>Total Courses: $numberOfCourses </td>
                <td>Total Instructors:  $numberOfInstructors</td>
            </tr>
            <tr>
                <td>Total Hours Assigned: $hoursAssigned</td>
                <td>Total Hours Left: $hoursLeft</td>
            </tr>
            <tr>
                <td>Number of Classes with higher ratio: $numberOfClassesWithHighRatio</td>
                <td>Number of Classes With Hours Left: $numberOfClassesWithHoursLeft</td>
            </tr>
            </table>
        </div>
    </div>
    <br>

    <a href="updateCourses.php"><button type="button" class="btn btn-success">Update Courses</button></a>
    <form action="{$_SERVER['PHP_SELF']}" method="post">
    <table id="dataTable" class="table table-bordered" >  
        <thead>
            <tr>  
                <th scope="col">Course name</th>    
                <th scope="col">No of Discussion Sections</th>  
                <th scope="col">Instructor</th>
                <th scope="col">No of Registered Students</th>
                <th scope="col">Ugrad TA hours Assigned </th>
                <th scope="col">Grad TA hours Assigned </th>
                <th scope="col">Hours Left</th>
                <th scope="col">Total Hours</th>
                <th scope="col">Additional Hours</th>
                <th scope="col">Admin Notes</th>
            </tr>
        </thead>
        <tbody>
HTML;


    $body .= $table . '</tbody></table>
        <input type="submit" name="updateCourse" class="btn btn-primary" value="Save">
    </form>';
}

function returnWarning($cond)
{
    if ($cond)
        return "table-warning";
    return "";
}

$breadcrumb = array("Home" => "../", "Admin" => "../index.php", "Courses" => "index.php");


$body .= <<<EOF
<br><br>
    <div class="card text-center" >
        <div class="card-body">
            <h4 class="card-title">Add a Course</h4>
            <form action="{$_SERVER['PHP_SELF']}" method="post">
                <table class="table card-text table-sm">
                    <tr>
                        <td>
                            <div >
                                <label for="addCourseName" class="col-form-label p-1">Course Name</label>
                                <input type="text" class="form-control" name="addCourseName"></input>
                            </div>
                        </td>
                        <td>
                            <div >
                                <label for="addDisSec" class="col-form-label p-1">Number of Discussion Sections</label>
                                <input type="text" class="form-control" name="addDisSec"></input>
                            </div>                            
                        </td>
                        <td>
                            <div >
                                <label for="addInstructor" class="col-form-label p-1">Instructor</label>
                                <input type="text" class="form-control" name="addInstructor"></input>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div >
                                <label for="addStudents" class="col-form-label p-1">Number of Students</label>
                                <input type="text" class="form-control" name="addStudents"></input>
                            </div>                           
                        </td>
                        <td>
                            <div >
                                <label for="addHours" class="col-form-label p-1">Hours</label>
                                <input type="text" class="form-control" name="addHours"></input>
                            </div>
                        </td>
                        <td>
                            <div >
                                <label for="addAdditionalHours" class="col-form-label p-1">Additional Hours</label>
                                <input type="text" class="form-control" name="addAdditionalHours"></input>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div >
                                <label for="addNotes" class="col-form-label p-1">Notes</label>
                                <input type="text" class="form-control" name="addNotes"></input>
                            </div>
                        </td>
                        <td>
                            <div>
                                <label for="updateCourse" class="col-form-label p-1">&nbsp;</label>
                                <input type="submit" name="addCourse" class="btn btn-primary form-control" value="Add Course">
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
                
            </form>
        </div>
    </div>
EOF;


generatePage($body, "Administrative Portal-Courses", "Courses", $breadcrumb);
