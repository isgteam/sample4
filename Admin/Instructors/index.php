<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');

$message = "";
$body = "";

if (isset($_POST['saveInstructors'])) {

    $name = $_POST['name'];
    $directoryId = $_POST['directoryId'];
    $oldName = $_POST['oldName'];


    for ($i = 0; $i < count($name); $i++) {
        if ($directoryId[$i] != "NONE") {

            $result = $database->update(
                "Instructors", 
                [   "Name" => trim($name[$i]),
                    "DirectoryId" => trim($directoryId[$i])
                ],
                ["Name" => $oldName[$i]]
            );

            // Show if your update to the DB was successful or not

            if ($result->errorInfo()[2] == null)
                $message = '<div class="alert alert-success" role="alert">
                    Your updates have been saved.  
                </div>';
            else
                $message = '<div class="alert alert-danger" role="alert">
                    An error occurred. Your updates were not saved.
                </div>';
            getDBErrors($result);
        }
    }

    $body .= $message;
}

$body .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';

// Make Instructors Table
$body .= '
<div class="table-responsive">
<table id="dataTable" class="table table-bordered table-striped table-sm" >  
<thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Directory ID</th>
        <th scope="col">Delete</th>

    </tr>
</thead>
<tbody>';

$body .= makeInstructorTable();

$body .= '</tbody></table></div>';

$body .= '
    <input type="submit" name="saveInstructors" class="btn btn-primary" formaction="index.php" formmethod="post" value="Save">
</form>';

$breadcrumb = array("Home" => "../../", "Admin" => "../", "Instructors" => "index.php");

generatePage($body, "Administrative Portal-Instructors", "Instructors", $breadcrumb);

function makeInstructorTable()
{
    global $database;
    $table="";

    $result = $database->select("Instructors", "*");

    foreach ($result as $row) {
        $table .= '<tr>
            <td>
                <input type="text" style="display:table-cell; width:80%" name="name[]" value="'.$row["Name"].'">
                <input type="text" name="oldName[]" value="' . $row['Name'] . '" hidden>
            </td>
            <td><input type="text" style="display:table-cell; width:80%" name="directoryId[]" value="'.$row["DirectoryId"].'"></td>
            <td><a href="deleteInstructor.php?Name='.$row['Name'] .'">Delete</a></td>
        </tr>';
    }

    return $table;
}
