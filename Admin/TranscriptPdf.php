<?php
/* This file is used to retrieve and download the transcript file from the DB. When the excel sheet is downloaded from the ugExport or the gExport page, in the transcript column there is a unique link for every applicant in the DB. It works as if you are giving a get request to this script and you can get the UID and the type of student the person is. We need the UID because that is the key in both the grad and ugrad tables also we need the type of student so that we know which table to look into.  */
require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/AccessDB/DBquery.php');

// Get UID from the URL
$uid = $_GET['UID'];

// Get type of the student from the URL
$type = strcmp($_GET['type'], "Ugrad") == 0  ? "Ugrad" : "Grad";

// Necessary Header calls for correct file output
// header("Content-Disposition: attachment; filename=".$uid.".pdf"); 
// header('Content-Type: application/octet-stream');
// header("Content-Transfer-Encoding: binary\n"); 

// Get the BLOB of the resume from th DB
$data = $database->select($type, "Transcript", ["UID[=]" => $uid ]);

echo '
<object height="100%" width="100%" type="application/pdf" data="data:application/pdf;base64,'.base64_encode($data[0]).'" ></object>';