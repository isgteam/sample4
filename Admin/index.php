<?php
/* This is the index page for the administrative portal and this shows the various options an administrator has over the application.
Right now the available options are:
1) Courses: View courses and hour-balances
2) Assign Class to TA: Manually assign TAs for classes
3) Assigned Students: View students already assigned to CS courses
4) Unassigned Students: View students pending assignment to courses
5) Export Undergraduate Data: Download Excel file for all Undergraduate
6) Export Graduate Data: Download Excel file for all Graduate

 */

require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/CASlogin/CASlogin.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');

if (strcmp(phpCAS::getUser(), "apoorv") != 0 && strcmp(phpCAS::getUser(), "thurst") != 0) {
	header("Location: ../Faculty/index.php");
}

// If the applications are closed
$json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/closed.json'), true);

$openOrClosed = "";
$mes = "";

if (isset($_POST['openorclose']) && $_POST['openorclose']) {
	if ($json['Closed']) {
		$json['Closed'] = false;
	} else {
		$json['Closed'] = true;
	}
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/closed.json', json_encode($json));
	unset($_POST['openorclose']);
}

if ($json['Closed']) {
	$openOrClosed = "Open Applications";
	$mes = "The applications are currently closed.";
} else {
	$openOrClosed = "Close Applications";
	$mes = "The applications are currently open.";
}

// see the semester
$sem = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/semester.json'), true);

if (isset($_POST['changeSem'])) {
	$sem['semester'] = $_POST['newSem'];
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/semester.json', json_encode($sem));
}


// For Dashboard
$ugradApplicants = $database->count("Ugrad");
$mastersApplicants = $database->count("Grad", ["gradStatus" => "MASTERS"]);
$phdApplicants = $database->count("Grad", ["gradStatus" => "PHD"]);
$totalApplicants = $ugradApplicants + $mastersApplicants + $phdApplicants;
$numberOfCourses = $database->count("Courses", "Name");
$numberOfInstructors = $database->count("Instructors");
$numberOfAssignedUgrad = $database->count("Ugrad", [
	"OR" => [
		"assignedClass[!]" => null,
		"fixedClass[!]" => ""
	]
]);

$whatSemester = '<form action="' . $_SERVER['PHP_SELF'] . '" method="post" class="form-horizontal">
<input type="text" name="newSem" id="newSem"  value="' . $sem['semester'] . '">
<input type="submit" name="changeSem" id="changeSem" class="btn btn-secondary" value="Save">
</form>';


$numberOfAssignedMasters = $database->count("Grad", [
	"AND" => [
		"OR" => [
			"assignedClass[!]" => null,
            "fixedClass[!]" => ""
		],
		"gradStatus" => "MASTERS"
	]
]);

$numberOfAssignedPhd = $database->count("Grad", [
	"AND" => [
		"OR" => [
			"assignedClass[!]" => null,
            "fixedClass[!]" => ""
		],
		"gradStatus" => "PHD"
	]
]);

$unassignedUgrads = ($ugradApplicants-$numberOfAssignedUgrad);
$unassignedMasters = ($mastersApplicants-$numberOfAssignedMasters);
$unassignedPhds = ($phdApplicants-$numberOfAssignedPhd);

// Creating an associative array for links in breadcrumbs
$breadcrumb = array("Home" => "../", "Admin" => ".");

$body = <<<HTML

<div class="card text-center" >
	<div class="card-body">
		<h4 class="card-title">Dashboard</h4>
		<table class="table card-text table-sm">
		<tr>
			<td>Total Courses: $numberOfCourses </td>
			<td>Total Instructors:  $numberOfInstructors</td>
			<td>Total Applicants: $totalApplicants</td>
		</tr>
		<tr>
			<td>Total Undergraduate applicants: $ugradApplicants</td>
			<td>Total Masters applicants: $mastersApplicants</td>
			<td>Total PhD applicants: $phdApplicants</td>
		</tr>
		<tr>
			<td>Number of Undergraduate TAs assigned: $numberOfAssignedUgrad</td>
			<td>Number of Masters TAs assigned: $numberOfAssignedMasters</td>
			<td>Number of PhD TAs assigned: $numberOfAssignedPhd</td>
		</tr>
		<tr>
			<td>Number of Undergraduate TAs unassigned: $unassignedUgrads</td>
			<td>Number of Masters TAs unassigned: $unassignedMasters</td>
			<td>Number of PhD TAs unassigned: $unassignedPhds</td>
		</tr>
		<tr>
			<td>Semester: </td>
			<td>$whatSemester</td>
			<td></td>
		</tr>
		</table>
	</div>
</div>

<div class="card-deck  mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">Add Students</h4>
			<p>Upload an excel file of students before running the system to add to DataBase.</p>

		</div>
		<div class="card-footer">
			<a href="upload.php" class="btn btn-lg btn-primary" role="button" name="upload" id="courses">Upload</a>
		</div>

	</div>
</div>

<div class="card-deck  mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">Add/View Courses</h4>
			<p>Add or View courses and hour-balances</p>
		</div>
		<div class="card-footer">
			<a href="Course/index.php" class="btn btn-lg btn-primary" role="button" name="courses" id="courses">Courses</a>
		</div>

	</div>
</div>

<div class="card-deck mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">Assign TA to Class</h4>
			<p>Automatically or Manually assign TAs for classes</p>
		</div>
		<div class="card-footer">
			<a href="Assign/assign.php" class="btn btn-lg btn-warning" role="button" name="Assign" id="Assign">Automatically Assign</a>
			<a href="Assign/index.php" class="btn btn-lg btn-primary" style="margin-right: -100px;" role="button" name="Assign" id="Assign">Manually Assign</a>
			<a href="Assign/setting.php" class="btn btn-light float-right" role="button" name="Setting" id="Setting">
				<i class="fas fa-cog"></i>Settings for <br>Auto Assign
			</a>
		</div>
		
	</div>
</div>

<div class="card-deck mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">Current TA Assignments</h4>
			<p>View or Export TAs assigned to CS courses or applicants not assigned anywhere</p>
		</div>
		<table class="table table-striped text-center">
			<tr>
				<td>
					<a href="assignedStudents.php" class="btn btn-lg btn-primary" role="button" name="assigned" id="assigned">Assigned Students</a>
					<a href="unassignedStudents.php" class="btn btn-lg btn-primary" role="button" name="unassigned" id="unassigned">Unassigned Students</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="Export/ugExport.php" class="btn btn-lg btn-primary" role="button" name="assigned" id="assigned">Export UnderGrad TA Data</a>
					<a href="Export/gExport.php" class="btn btn-lg btn-primary" role="button" name="assigned" id="assigned">Export Grad TA Data</a>
				</td>
			</tr>
		</table>

	</div>
</div>

<div class="card-deck mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">Open/Close Applications</h4>
			<p>Open or Close Applications. $mes</p>
		</div>
		<div class="card-footer">
			<form action="{$_SERVER['PHP_SELF']}" method="post" class="form-horizontal">
				<input type="submit" name="openorclose" id="openorclose" class="btn btn-lg btn-primary" value="{$openOrClosed}">
			</form>
		</div>
		
	</div>
</div>

<div class="card-deck mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">Remove all applicants</h4>
			<p>This will empty the database and remove all applicants. Please contact IT to restore the DB.</p>
		</div>
		<div class="card-footer">
			<a href="delete.php" class="btn btn-lg btn-danger" role="button" onClick="javascript: return confirm('Please confirm deletion');" name="Delete" id="Delete">Delete All applicants</a>
		</div>
		
	</div>
</div>


<div class="card-deck  mt-2">
	<div class="card border-dark text-center">

		<div class="card-block mt-2">
			<h4 class="card-title">View/Edit Instructors</h4>
			<p>Edit Instructors in the Data Base</p>

		</div>
		<div class="card-footer">
			<a href="Instructors/index.php" class="btn btn-lg btn-primary" role="button" name="upload" id="courses">Edit Instructors</a>
		</div>

	</div>
</div>

HTML;

generatePage($body, "Administrative Portal", "Administrative Options", $breadcrumb = $breadcrumb);
