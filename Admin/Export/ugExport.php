<?php
/* This file is used to dump all the data from the ugrad table to an excel file.   */
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$data = $database->select("Ugrad", "*");

$headings = array();

foreach(array_keys($data[0]) as $k){
    array_push($headings, $tableDisplay[$k]);
}

$arrayData = [
    array_values($headings)
];

$transcriptLinks = [];

foreach ($data as $row) {
    $row['Transcript'] = 'Transcript';
    array_push($transcriptLinks, "http://taapp.cs.umd.edu/Admin/TranscriptPdf.php?UID=" . $row['UID'] . "&type=Ugrad");
    array_push($arrayData, $row);
}

$sheet->fromArray(
    $arrayData,
    'A1'
);

// Actually hyperlinking the transcript
for ($rows = 0; $rows < count($transcriptLinks); $rows++) {
    $sheet->getCell('L' . ($rows + 2))->getHyperlink()->setUrl($transcriptLinks[$rows]);
    $sheet->getStyle('L' . ($rows + 2))->applyFromArray(array('font' => array('color' => ['rgb' => '0000FF'], 'underline' => 'single')));
}

// Auto Sizing the columns in the excel sheet
for ($cols = 0; $cols < count($arrayData[0]); $cols++) {
    $temp = chr($cols + 65);
    if (($cols + 65) > 90)
        $temp = 'A' . chr($cols + 65 - 26);
    if($cols/26 >= 2)
        $temp = 'B' . chr($cols%26 + 65);
    $sheet->getColumnDimension($temp)->setAutoSize(true);
}

// Save the file directly to php output
$writer = new Xls($spreadsheet);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="UndergraduateTAs.xls"');
header("Pragma: no-cache");
header("Expires: 0");
$writer->save("php://output");
exit;
