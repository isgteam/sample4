<?php
/* This is page will delete applicant from the database.*/

require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php');

// Using Medoo namespace
use Medoo\Medoo;

$database->delete("Grad", Medoo::raw('WHERE TRUE'));
$database->delete("Ugrad",Medoo::raw('WHERE TRUE'));
echo "<script>alert('All applicants deleted');window.location = '/Admin/index.php';</script>";