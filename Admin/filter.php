<?php

namespace Filter;

/* This script is a helper for adding the filter option in any place you see tables. */

$fieldsToDisplay = array("FirstName", "LastName", "UID", "gradStatus", "assignedClass", "assignedInstructor", "fixedClass", "fixedInstructor");

// This is a associative array which has DB column names mapped to the actual Table Column heading you want to display. For example for the col fixedClass in DB show what it means that is "Manually Assigned Class"
$tableDisplay = array(
    "Date" => 'Time Applied', "FirstName" => "First Name", "LastName" => "Last Name", "Phone" => "Phone", "Email" => "Email", "DirectoryId" => "DirectoryId", "UID" => "UID", "GPA" => "GPA", "Semesters" => "Semesters", "Department" => "Department",  "gradStatus" => "Type", "Transcript" => "Transcript",
    "Masters" => "Masters", "Semester" => "Semester", "Year" => "Year", "Advisor" => "Advisor", "Currta" => "Is currently a TA?", "Step" => "Step", "Course" => "Course",
    "Instructor" => "Instructor",
    "Course1TAed" => "Course1 TAed for",
    "Course1TAedOverlap" => "Is 50% or more overlap for Course1?",
    "Course1TAedGrade" => "Course1 TAed Grade",
    "Course1TAedTook" => "Course1 TAed Took",
    "Course1TAedInstructor" => "Course1TAedInstructor",
    "Course2TAed" => "Course2 TAed",
    "Course2TAedOverlap" => "Is 50% or more overlap for Course2?",
    "Course2TAedGrade" => "Course2 TAed Grade",
    "Course2TAedTook" => "Course2 TAed Took",
    "Course2TAedInstructor" => "Course2TAedInstructor",
    "Course3TAed" => "Course3 TAed",
    "Course3TAedOverlap" => "Is 50% or more overlap for Course2?",
    "Course3TAedGrade" => "Course3 TAed Grade",
    "Course3TAedTook" => "Course3 TAed Took",
    "Course3TAedInstructor" => "Course3 TAed Instructor",
    "Mei" => "MEI",
    "Umei" => "UMEI",
    "Position" => "Position",
    "preferredSemester" => "preferredSemester",
    "preferredYear" => "preferredYear",
    "Course1TAed" => "TAed for 1st course", "Course1TAedTook" => "TAed for 1st course in", "Course1TAedInstructor" => "TAed for 1st course Instructor",
    "Course2TAed" => "TAed for 2nd course", "Course2TAedTook" => "TAed for 2nd course in", "Course2TAedInstructor" => "TAed for 2nd course Instructor",
    "Course3TAed" => "TAed for 3rd course", "Course3TAedTook" => "TAed for 3rd course in", "Course3TAedInstructor" => "TAed for 3rd course Instructor",
    "Choice_1" => "Choice 1", "Choice_1Grade" => "Grade of Choice 1 Class", "Choice_1Instructor" => "Instructor with whom Choice 1 taken", "Choice_1Took" => "Semester when Choice 1 was taken",
    "Choice_2" => "Choice 2", "Choice_2Grade" => "Grade of Choice 2 Class", "Choice_2Instructor" => "Instructor with whom Choice 2 taken", "Choice_2Took" => "Semester when Choice 2 was taken",
    "Choice_3" => "Choice 3", "Choice_3Grade" => "Grade of Choice 3 Class", "Choice_3Instructor" => "Instructor with whom Choice 3 taken", "Choice_3Took" => "Semester when Choice 3 was taken",
    "Choice_4" => "Choice 4", "Choice_4Grade" => "Grade of Choice 4 Class", "Choice_4Instructor" => "Instructor with whom Choice 4 taken", "Choice_4Took" => "Semester when Choice 4 was taken",
    "Choice_5" => "Choice 5", "Choice_5Grade" => "Grade of Choice 5 Class", "Choice_5Instructor" => "Instructor with whom Choice 5 taken", "Choice_5Took" => "Semester when Choice 5 was taken",
    "Comments" => "Comments",
    "course1TAed" => "course1TAed", "course1TAedTook" => "course1TAedTook", "course1TAedInstructor" => "course1TAedInstructor", "course2TAed" => "course2TAed", "course2TAedTook" => "course2TAedTook", "course2TAedInstructor" => "course2TAedInstructor", "course3TAed" => "course3TAed", "course3TAedTook" => "course3TAedTook", "course3TAedInstructor" => "course3TAedInstructor",
    "assignedClass" => "Automatically Assigned Class", "assignedInstructor" => "Automatically Assigned Instructor",
    "fixedClass" => "Manually Assigned Class", "fixedInstructor" => "Manually Assigned Instructor", "Date_Assigned" => 'Date Assigned', "HALF_TIME" => "Half Time", "ADMIN_NOTES" => "Admin Notes"
);

define('GRAD_TABLE', array(
    "Date", "FirstName", "LastName", "Phone", "Email", "UID", "DirectoryId", "GPA", "Semesters", "Department", "Transcript", "Choice_1", "Choice_2", "Choice_3", "Choice_4", "Choice_5", "Comments", "gradStatus", "Masters", "Semester", "Year", "Advisor", "Currta", "Step", "Course", "Instructor", "Course1TAed", "Course1TAedOverlap", "Course1TAedGrade", "Course1TAedTook", "Course1TAedInstructor", "Course2TAed", "Course2TAedOverlap", "Course2TAedGrade", "Course2TAedTook", "Course2TAedInstructor", "Course3TAed", "Course3TAedOverlap", "Course3TAedGrade", "Course3TAedTook", "Course3TAedInstructor", "Mei", "Umei", "Position", "preferredSemester", "preferredYear", "assignedClass", "assignedInstructor", "fixedClass", "fixedInstructor", "Date_Assigned", "HALF_TIME", "ADMIN_NOTES"
));


define('UGRAD_TABLE', array(
    "Date", "FirstName", "LastName", "Phone", "Email", "DirectoryId", "UID", "GPA", "Semesters", "Department", "gradStatus", "Transcript", "course1TAed", "course1TAedTook", "course1TAedInstructor", "course2TAed", "course2TAedTook", "course2TAedInstructor", "course3TAed", "course3TAedTook", "course3TAedInstructor", "Choice_1Took", "Choice_1Grade", "Choice_1Instructor", "Choice_2Took", "Choice_2Grade", "Choice_2Instructor", "Choice_3Took", "Choice_3Grade", "Choice_3Instructor", "Choice_4Took", "Choice_4Grade", "Choice_4Instructor", "Choice_5Took", "Choice_5Grade", "Choice_5Instructor", "Choice_1", "Choice_2", "Choice_3", "Choice_4", "Choice_5", "Comments", "assignedClass", "assignedInstructor", "fixedClass", "fixedInstructor", "Date_Assigned", "HALF_TIME", "ADMIN_NOTES"
));


if (isset($_POST['FilterButton'])) {
    $fieldsToDisplay = $_POST['filter'];
}

$filterdata = "";
$temp = 1;
foreach ($tableDisplay as $key => $value) {
    if ($temp % 2 == 0) {
        $filterdata .= '<tr>';
    }

    $filterdata .= '<td>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="filter[]" value="' . $key . '" ' . isChecked($key) . '> 
            <label class="form-check-label" for="Name">
            ' . $value . '
            </label>
        </div>
    </td>';

    if ($temp % 2 != 0) {
        $filterdata .= '</tr>';
    }
    $temp++;
}

$body = '
    <a href="#filter" class="btn btn-info" data-toggle="collapse">Options to filter</a>
    <div id="filter" class="collapse col text-left">
        <form action="' . $_SERVER['PHP_SELF'] . '" method="post">
        <table class="table table-sm" >
            ' . $filterdata . '
        </table>
        <input type="submit" name="FilterButton" class="btn btn-success" value="Filter">
        </form>
    </div> 
    <br/>
    <br/>';

// See what fields are to be displayed and check then in the checkbox if there are being shown
function isChecked($ele)
{
    global $fieldsToDisplay;
    if (in_array($ele, $fieldsToDisplay)) {
        return "checked";
    } else {
        return "";
    }
}
