<?php
/* This is the confirmation Page. This page comes after the review page. This page actually sends sends the ugrad or grad application data to be put in the database.*/

// Load Composer's autoloader
require_once('vendor/autoload.php');

// Import libraries to send confirmation email.
// Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;

///PHPMailer Object
$mail = new PHPMailer;

require_once($_SERVER['DOCUMENT_ROOT'].'/Student/Student.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Student/gradStudent.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/AccessDB/DBquery.php');
require_once('support.php');
session_start();

// Get the session variable applicant to know what type of student in applying
$applicant = $_SESSION['applicant'];

// Data to be inserted put in an associative array
$dataToInsert = array(
	"FirstName" => $applicant->getFirstName(),
	"LastName" => $applicant->getLastName(),
	"Phone" => $applicant->getPhone(),
	"Email" => $applicant->getEmail(),	
	"DirectoryId" => $applicant->getDirectoryId(),	
	"GPA" => $applicant->getGPA(),
	"Semesters" => $applicant->getSemesters(),
	"Department" => $applicant->getDepartment(),
	"Transcript" => $applicant->getTranscript(),
	"Course1TAed" => $applicant->getCourse1TAed(),
	"Course1TAedTook" => $applicant->getCourse1TAedTook(),
	"Course1TAedInstructor" => $applicant->getCourse1TAedInstructor(),
	"Course2TAed" => $applicant->getCourse2TAed(),
	"Course2TAedTook" => $applicant->getCourse2TAedTook(),
	"Course2TAedInstructor" => $applicant->getCourse2TAedInstructor(),
	"Course3TAed" => $applicant->getCourse3TAed(),
	"Course3TAedTook" => $applicant->getCourse3TAedTook(),
	"Course3TAedInstructor" => $applicant->getCourse3TAedInstructor(),
	"Choice_1" => $applicant->getCourse1(),
	"Choice_2" => $applicant->getCourse2(),
	"Choice_3" => $applicant->getCourse3(),
	"Choice_4" => $applicant->getCourse4(),
	"Choice_5" => $applicant->getCourse5(),
	"Comments" => $applicant->getComments(),
);

/* This checks if the student at the review page is undergraduate or graduate as to ensure that the data of the student goes into the correct table in the database */ 
if (strcmp($_SESSION['Student_type'],"Ugrad") ==  0){
	/*Query to insert the undergraduate  student into the Ugrad table*/ 
	$dataToInsert += array(
		"Choice_1Took" => $applicant->getCourse1Took(),
		"Choice_1Grade" => $applicant->getCourse1Grade(),
		"Choice_1Instructor" => $applicant->getCourse1TookInstructor(),
		"Choice_2Took" => $applicant->getCourse2Took(),
		"Choice_2Grade" => $applicant->getCourse2Grade(),
		"Choice_2Instructor" => $applicant->getCourse2TookInstructor(),
		"Choice_3Took" => $applicant->getCourse3Took(),
		"Choice_3Grade" => $applicant->getCourse3Grade(),
		"Choice_3Instructor" => $applicant->getCourse3TookInstructor(),
		"Choice_4Took" => $applicant->getCourse4Took(),
		"Choice_4Grade" => $applicant->getCourse4Grade(),
		"Choice_4Instructor" => $applicant->getCourse4TookInstructor(),
		"Choice_5Took" => $applicant->getCourse5Took(),
		"Choice_5Grade" => $applicant->getCourse5Grade (),
		"Choice_5Instructor" => $applicant->getCourse5TookInstructor(),
	);
}
else {
	/*Query to insert the Graduate  student into the Gradtable*/ 
	$dataToInsert += array(
		"gradStatus" => $applicant->getGradstatus(),
		"Masters" => $applicant->getMasters(),
		"Semester" => $applicant->getSemester(),
		"Year" => $applicant->getYear(),
		"Advisor" => $applicant->getAdvisor(),
		"Currta" => $applicant->getCurrta(),
		"Step" => $applicant->getStep(),
		"Course" => $applicant->getCourse(),
		"Instructor" => $applicant->getInstructor(),
		"Course1TAedOverlap" => $applicant->getCourse1TAedOverlap(),
		"Course1TAedGrade" => $applicant->getCourse1TAedGrade(),
		"Course2TAedOverlap" => $applicant->getCourse2TAedOverlap(),
		"Course2TAedGrade" => $applicant->getCourse2TAedGrade(),
		"Course3TAedOverlap" => $applicant->getCourse3TAedOverlap(),
		"Course3TAedGrade" => $applicant->getCourse3TAedGrade(),
		"Mei" => $applicant->getMei(),
		"Umei" => $applicant->getUmei(),
		"Position" => $applicant->getPosition(),
		"preferredSemester" => $applicant->getPreferredsemester(),
		"preferredYear" => $applicant->getPreferredyear()
	);
}

// Query insert into DB or update in it
if ($database->has($_SESSION['Student_type'], [ "UID[=]" => $applicant->getUID()])){
	$temp =  $database->update($_SESSION['Student_type'],$dataToInsert,["UID[=]" => $applicant->getUID()]);
	getDBErrors($temp);
}
else {
	$dataToInsert += array("UID" => $applicant->getUID());
	$temp = $database->insert($_SESSION['Student_type'],$dataToInsert);
	getDBErrors($temp);
}


//From email address and name
$mail->From = "no-reply@taapp.cs.umd.edu";
$mail->FromName = "TA Application System ";

//To address and name
$mail->addAddress($applicant->getEmail(), $applicant->getFirstName());

//Address to which recipient will reply
$mail->addReplyTo("no-reply@taapp.cs.umd.edu", "Do Not Reply");

//CC and BCC
// $mail->addCC("cc@example.com");
// $mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);

$mail->Subject = "TA Application for CS Department";
$mail->Body = "<p>Hi ".$applicant->getFirstName().",</p>
<p>Thank you for applying to the Teaching Assistant position at Computer Science Department, University of Maryland. If you want to change the data provided please submit the application again.</p>
<p>Best Regards,</p>
<p>Computer Science Department, UMD</p>";
$mail->AltBody = "Thank you for applying to the Teaching Assistant position at Computer Science Department, University of Maryland. If you want to change the data provided please submit the application again.";

$mailStatus = "";

if(!$mail->send()) {
	$mailStatus = "Mailer Error: " . $mail->ErrorInfo;
} 
else {
	$mailStatus = "Message has been sent successfully";
}

echo "<script type='text/javascript'>console.log('".$mailStatus."');</script>";

// Show that the application data has been submitted
$body = '<p class="text-center">Your application has been submitted. An confirmation email has been send to '.$applicant->getEmail().'. If you want to change the data provided please submit the application again. Please check Spam/Junk folder if email is not received.</p>';

generatePage($body,"Application Submitted","Thank You");
