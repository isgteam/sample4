<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/Student/Student.php');

class gradStudent extends Student
{
    protected $firstname, $lastname, $mi, $phone, $email,$directoryid, $uid, $gpa,
        $semesters, $department, $transcript, $prevta, $course1, $course2,
        $course3, $course4, $course5, $comments,
        $gradstatus, $masters, $semester, $year, $advisor, $currta,
        $step, $course, $instructor, $course1TAed,
        $course1TAedOverlap, $course1TAedGrade, $course1TAedTook, $course1TAedInstructor, $course2TAed, $course2TAedOverlap, $course2TAedGrade, $course2TAedTook, $course2TAedInstructor, $course3TAed, $course3TAedOverlap, $course3TAedGrade, $course3TAedTook, $course3TAedInstructor, $mei, $umei, $position,
        $preferredsemester, $preferredyear;

    public function __construct(
        string $firstname,
        string $lastname,
        string $mi,
        string $phone,
        string $email,
        string $uid,
        string $directoryid,
        string $gpa,
        string $semesters,
        string $department,
        $transcript,
        string $prevta,
        string $course1,
        string $course2,
        string $course3,
        string $course4,
        string $course5,
        string $comments,
        string $gradstatus,
        string $masters,
        string $semester,
        string $year,
        string $advisor,
        string $currta,
        string $step,
        string $course,
        string $instructor,
        string $course1TAed,
        string $course1TAedOverlap,
        string $course1TAedGrade,
        string $course1TAedTook,
        string $course1TAedInstructor,
        string $course2TAed,
        string $course2TAedOverlap,
        string $course2TAedGrade,
        string $course2TAedTook,
        string $course2TAedInstructor,
        string $course3TAed,
        string $course3TAedOverlap,
        string $course3TAedGrade,
        string $course3TAedTook,
        string $course3TAedInstructor,
        string $mei,
        string $umei,
        string $position,
        string $preferredsemester,
        string $preferredyear
    ) {

        parent::__construct(
            $firstname,
            $lastname,
            $mi,
            $phone,
            $email,
            $uid,
            $directoryid,
            $gpa,
            $semesters,
            $department,
            $transcript,
            $prevta, // yes or no 
            $course1TAed,
            $course1TAedTook,
            $course1TAedInstructor,

            $course2TAed,
            $course2TAedTook,
            $course2TAedInstructor,

            $course3TAed,
            $course3TAedTook,
            $course3TAedInstructor,
            $course1,
            "",
            "",
            "",
            $course2,
            "",
            "",
            "",
            $course3,
            "",
            "",
            "",
            $course4,
            "",
            "",
            "",
            $course5,
            "",
            "",
            "",
            $comments
        );


        $this->gradstatus = $gradstatus; // phd or ms
        $this->masters = $masters; // yes or no
        $this->semester = $semester;
        $this->year = $year; // 2011 to 2018 
        $this->advisor = $advisor;
        $this->currta = $currta; //  ta now yes or no 
        $this->step = $step; // step 1 or Step II or step III
        $this->course = $course; // previous ta for 
        $this->instructor = $instructor; // empty field 
        $this->mei = $mei; // yes or no 
        $this->umei = $umei; // yes or no 
        $this->position = $position; // full time or part time 
        $this->preferredsemester = $preferredsemester; // fall spring summer 
        $this->preferredyear = $preferredyear; // 2018 or 2019
        $this->course1TAedOverlap = $course1TAedOverlap;
        $this->course1TAedGrade = $course1TAedGrade;
        $this->course2TAedOverlap = $course2TAedOverlap;
        $this->course2TAedGrade = $course2TAedGrade;
        $this->course3TAedOverlap = $course3TAedOverlap;
        $this->course3TAedGrade = $course3TAedGrade;
    }

    /* A To string method which prints out all the variables of 
    Student class */
    public function __toString()
    {
        return parent::__toString() . " Gradstatus: " . $this->gradstatus .
            "Master: " . $this->masters .
            "Semester: " . $this->semester .
            "Year: " . $this->year .
            "Advisor: " . $this->advisor .
            "Currta: " . $this->currta .
            "Step: " . $this->step .
            "Course: " . $this->course .
            "Instructor: " . $this->instructor .
            "Mei: " . $this->mei .
            "Umei: " . $this->umei .
            "Position: " . $this->position .
            "Preferredsemester: " . $this->preferredsemester .
            "Preferredyear: " . $this->preferredyear .
            "Course1TAedOverlap: " . $this->course1TAedOverlap .
            "Course1TAedGrade: " . $this->course1TAedGrade .
            "Course2TAedOverlap: " . $this->course2TAedOverlap .
            "Course2TAedGrade: " . $this->course2TAedGrade .
            "Course3TAedOverlap: " . $this->course3TAedOverlap .
            "Course3TAedGrade: " . $this->course3TAedGrade;
    }

    public function getGradstatus()
    {
        return $this->gradstatus;
    }

    public function getMasters()
    {
        return $this->masters;
    }

    public function getSemester()
    {
        return $this->semester;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getAdvisor()
    {
        return $this->advisor;
    }

    public function getCurrta()
    {
        return $this->currta;
    }

    public function getStep()
    {
        return $this->step;
    }

    public function getCourse()
    {
        return $this->course;
    }

    public function getInstructor()
    {
        return $this->instructor;
    }

    public function getCourse1TAedOverlap()
    {
        return $this->course1TAedOverlap;
    }
    public function getCourse1TAedGrade()
    {
        return $this->course1TAedGrade;
    }
    public function getCourse2TAedOverlap()
    {
        return $this->course2TAedOverlap;
    }
    public function getCourse2TAedGrade()
    {
        return $this->course2TAedGrade;
    }
    public function getCourse3TAedOverlap()
    {
        return $this->course3TAedOverlap;
    }
    public function getCourse3TAedGrade()
    {
        return $this->course3TAedGrade;
    }

    public function getMei()
    {
        return $this->mei;
    }

    public function getUmei()
    {
        return $this->umei;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getPreferredsemester()
    {
        return $this->preferredsemester;
    }

    public function getPreferredyear()
    {
        return $this->preferredyear;
    }
}
