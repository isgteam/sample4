<?php

class Student
{
		protected $firstname, $lastname, $mi, $phone, $email, $uid,$directoryid, $gpa,
		$semesters, $department, $transcript, $prevta, $course1TAed, $course1TAedTook, $course1TAedInstructor, $course2TAed, $course2TAedTook, $course2TAedInstructor, $course3TAed, $course3TAedTook, $course3TAedInstructor, $course1, $course1Took, $course1Grade, $course1TookInstructor, $course2, $course2Took, $course2Grade, $course2TookInstructor, $course3, $course3Took, $course3Grade, $course3TookInstructor, $course4, $course4Took, $course4Grade, $course4TookInstructor, $course5, $course5Took, $course5Grade, $course5TookInstructor, $comments;

	public function __construct(
		$firstname,
		$lastname,
		$mi, 
		$phone, 
		$email, 
		$directoryid,
		$uid,
		$gpa,
		$semesters, 
		$department, 
		$transcript, 
		$prevta,
		$course1TAed,
		$course1TAedTook,
		$course1TAedInstructor,

		$course2TAed,
		$course2TAedTook,
		$course2TAedInstructor,

		$course3TAed,
		$course3TAedTook,
		$course3TAedInstructor,

		$course1,
		$course1Took,
		$course1Grade,
		$course1TookInstructor,

		$course2,
		$course2Took,
		$course2Grade,
		$course2TookInstructor,

		$course3,
		$course3Took,
		$course3Grade,
		$course3TookInstructor,

		$course4,
		$course4Took,
		$course4Grade,
		$course4TookInstructor,

		$course5, 
		$course5Took,
		$course5Grade,
		$course5TookInstructor,
		$comments
	) {

			$this->firstname = $firstname;
			$this->lastname = $lastname;
			$this->mi = $mi;
			$this->phone = $phone;
			$this->email = $email;
			$this->directoryid = $directoryid;
			$this->uid = $uid;
			$this->gpa = $gpa;
			$this->semesters = $semesters;
			$this->department = $department;
			$this->transcript = $transcript;
			$this->prevta = $prevta;
			$this->course1TAed = $course1TAed;
			$this->course1TAedTook = $course1TAedTook;
			$this->course1TAedInstructor = $course1TAedInstructor;

			$this->course2TAed = $course2TAed;
			$this->course2TAedTook = $course2TAedTook;
			$this->course2TAedInstructor = $course2TAedInstructor;

			$this->course3TAed = $course3TAed;
			$this->course3TAedTook = $course3TAedTook;
			$this->course3TAedInstructor = $course3TAedInstructor;

			$this->course1 = $course1;
			$this->course1Took = $course1Took;
			$this->course1Grade = $course1Grade;
			$this->course1TookInstructor = $course1TookInstructor;

			$this->course2 = $course2;
			$this->course2Took = $course2Took;
			$this->course2Grade = $course2Grade;
			$this->course2TookInstructor = $course2TookInstructor;

			$this->course3 = $course3;
			$this->course3Took = $course3Took;
			$this->course3Grade = $course3Grade;
			$this->course3TookInstructor = $course3TookInstructor;

			$this->course4 = $course4;
			$this->course4Took = $course4Took;
			$this->course4Grade = $course4Grade;
			$this->course4TookInstructor = $course4TookInstructor;

			$this->course5 = $course5;
			$this->course5Took = $course5Took;
			$this->course5Grade = $course5Grade;
			$this->course5TookInstructor = $course5TookInstructor;
			$this->comments = $comments;
		}

		/* A To string method which prints out all the variables of 
		Student class */
		public function __toString() {
			return "First Name: ".$this->firstname.
			" Last Name: ".$this->lastname.
			" UID: ".$this->uid .
			" Phone: ".$this->phone.
			" Email: ".$this->email.
			" DirectoryID: ".$this->directoryid.
			" GPA: ".$this->gpa .
			" Semesters: ".$this->semesters .
			" Department: ".$this->department .
			" Transcript: ".$this->transcript.
			" Prevta: ".$this->prevta .
			" Course1 TAed: ".$this->course1TAed.
			" Course1 TAed in: ".$this->course1TAedTook.
			" Course1 TAed Instructor: ".$this->course1TAedInstructor.

			" Course2 TAed: ".$this->course2TAed.
			" Course2 TAed in: ".$this->course2TAedTook.
			" Course2 TAed Instructor: ".$this->course2TAedInstructor.

			" Course3 TAed: ".$this->course3TAed.
			" Course3 TAed in: ".$this->course3TAedTook.
			" Course3 TAed Instructor: ".$this->course3TAedInstructor.

			" Course1: ".$this->course1.
			" Course1 took in: ".$this->course1Took.
			" Course1 Grade: ".$this->course1Grade.
			" Course1 Taken Instructor:".$this->course1TookInstructor.

			" Course2: ".$this->course2.
			" Course2 took in: ".$this->course2Took.
			" Course2 Grade: ".$this->course2Grade.
			" Course2 Taken Instructor: ".$this->course2TookInstructor.

			" Course3: ".$this->course3.
			" Course3 took in: ".$this->course3Took.
			" Course3 Grade: ".$this->course3Grade.
			" Course3 Taken Instructor: ".$this->course3TookInstructor.

			" Course4: ".$this->course4.
			" Course4 took in: ".$this->course4Took.
			" Course4 Grade: ".$this->course4Grade.
			" Course4 Taken Instructor: ".$this->course4TookInstructor.

			" Course5: ".$this->course5.
			" Course5 took in: ".$this->course5Took.
			" Course5 Grade: ".$this->course5Grade.
			" Course5 Taken Instructor: ".$this->course5TookInstructor.
			" Comments: ".$this->comments ;
		}

		public function getFirstName() {
			return $this->firstname;
		}

		public function getLastName(){
			return $this->lastname;
		}

		public function getUID() {
			return $this->uid;
		}

		public function getPhone() {
			return $this->phone;
		}

		public function getEmail() {
			return $this->email;
		}

		public function getDirectoryId() {
			return $this->directoryid;
		}

		public function getGPA() {
			return $this->gpa;
		}

		public function getSemesters() {
			return $this->semesters;
		}

		public function getDepartment() {
			return $this->department;
		}

		public function getTranscript() {
			return $this->transcript;
		}

		public function getPrevTA() {
			return $this->prevta;
		}

		public function getCourse1TAed() {
			return $this->course1TAed;
		}
		public function getCourse1TAedTook() {
			return $this->course1TAedTook;
		}
		public function getCourse1TAedInstructor() {
			return $this->course1TAedInstructor;
		}

		public function getCourse2TAed() {
			return $this->course2TAed;
		}
		public function getCourse2TAedTook() {
			return $this->course2TAedTook;
		}
		public function getCourse2TAedInstructor() {
			return $this->course2TAedInstructor;
		}

		public function getCourse3TAed() {
			return $this->course3TAed;
		}
		public function getCourse3TAedTook() {
			return $this->course3TAedTook;
		}
		public function getCourse3TAedInstructor() {
			return $this->course3TAedInstructor;
		}

		public function getCourse1() {
			return $this->course1;
		}
		public function getCourse1Took() {
			return $this->course1Took;
		}
		public function getCourse1Grade() {
			return $this->course1Grade;
		}
		public function getCourse1TookInstructor() {
			return $this->course1TookInstructor;
		}

		public function getCourse2() {
			return $this->course2;
		}
		public function getCourse2Took() {
			return $this->course2Took;
		}
		public function getCourse2Grade() {
			return $this->course2Grade;
		}
		public function getCourse2TookInstructor() {
			return $this->course2TookInstructor;
		}

		public function getCourse3() {
			return $this->course3;
		}
		public function getCourse3Took() {
			return $this->course3Took;
		}
		public function getCourse3Grade() {
			return $this->course3Grade;
		}
		public function getCourse3TookInstructor() {
			return $this->course3TookInstructor;
		}

		public function getCourse4() {
			return $this->course4;
		}
		public function getCourse4Took() {
			return $this->course4Took;
		}
		public function getCourse4Grade() {
			return $this->course4Grade;
		}
		public function getCourse4TookInstructor() {
			return $this->course4TookInstructor;
		}

		public function getCourse5() {
			return $this->course5;
		}
		public function getCourse5Took() {
			return $this->course5Took;
		}
		public function getCourse5Grade () {
			return $this->course5Grade;
		}
		public function getCourse5TookInstructor() {
			return $this->course5TookInstructor;
		}

		public function getComments() {
			return $this->comments;
		}



	}
?>