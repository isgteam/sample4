<?php
/* This is a support file which is used to render the page, show errors in console, or on the page and has various customizable features. There is a separate support file in every folder so that the assets can be easily accessed.*/

// function to generate a basic page and show php errors in the end
function generatePage($body, $title = "CMSC TA Application Site", $PageTitle = "", $breadcrumb = array(), $css = "", $includeBootstrap = true, $jsIncludes = "")
{
    $date = date("Y");

    // Making links for BreadCrumbs
    $breadcrumbs = null;
    if (count($breadcrumb) != 0) {
        $breadcrumbs = '<nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="background-color: #e8e8e8">
                ';
        foreach ($breadcrumb as $name => $link) {
            $breadcrumbs .= '<li class="breadcrumb-item"><a href="' . $link . '">' . $name . '</a></li>';
        }
        $breadcrumbs .= '</ol></nav>';
    }

    // See if you have to include Bootstrap or not
    $bootstrap = $includeBootstrap ?
        '<link rel="stylesheet" href="//' . $_SERVER['SERVER_NAME'] . '/Assets/Bootstrap/bootstrap.min.css">
        <script src="//' . $_SERVER['SERVER_NAME'] . '/Assets/Bootstrap/jquery.min.js"></script>
        <script src="//' . $_SERVER['SERVER_NAME'] . '/Assets/Bootstrap/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//' . $_SERVER['SERVER_NAME'] . '/Assets/Bootstrap/datatables.min.css"/>
        <script type="text/javascript" src="//' . $_SERVER['SERVER_NAME'] . '/Assets/Bootstrap/datatables.jquery.js"></script>
        <script type="text/javascript" src="//' . $_SERVER['SERVER_NAME'] . '/Assets/Bootstrap/datatables.bootstrap.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">' : '';

    $baseurl = '//' . $_SERVER['SERVER_NAME'];

    // The basic page layout
    $page = <<<HTML
<!doctype html>
<html lang="en">
    <head> 
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>$title</title>

        <!-- Bootstrap CSS -->
        $bootstrap

        <!-- CSS Custom -->
        $css
        <style>
        html {
            height: 100%;
            box-sizing: border-box;
        }

        *,
        *:before,
        *:after {
            box-sizing: inherit;
        }

        body {
            position: relative;
            margin: 0;
            padding-bottom: 8rem;
            min-height: 100%;
        }

        .footer {
            position: absolute;
            right: 0;
            bottom: 0;
            left: 0;
            padding: 1rem;
        }

        #dataTable {
            width: auto !important;
        }
        </style>
    </head>
            
    <body>
        <script src="https://umd-header.umd.edu/build/bundle.js?search=0&amp;search_domain=&amp;events=0&amp;news=0&amp;schools=0&amp;admissions=0&amp;support=0&amp;support_url=&amp;wrapper=0&amp;sticky=0"></script>
        <noscript><br />
            <div class="um-brand pb-0 mb-0">
            <div id="logo">
                <a href="cs.umd.edu" target="_blank">University of Maryland</a>
            </div>
            </div>
        </noscript>
        
        <nav class="navbar navbar-dark shadow-sm p-3 rounded" style="background-color: #e8e8e8">
            <img class="navbar-brand img-fluid col-5 in-line" src="$baseurl/Assets/logo.png">
            <h3 class="text-fluid text-right">Teaching Assistant Application System</h3>
        </nav>
        $breadcrumbs

        <h4 class="text-center display-4" >$PageTitle</h4>

        <div class="container-fluid">
            $body
        </div>


        <!-- Footer of every page -->
        <footer class="footer page-footer font-small mt-4 pt-2" style="background-color: #e8e8e8; ">

            <div class="container-fluid text-center">
            <a href="https://cs.umd.edu">UMD CS</a>
            </div>
    
            <div class="footer-copyright text-center py-3">@ $date Copyright: 
            <a href="https://www.umd.edu">University of Maryland</a> 
            </div>
    
        </footer>

    </body>

    <!-- JS Custom -->
        $jsIncludes
        <script>
            $(document).ready(function() {
                $('#dataTable').DataTable({
                    "fixedHeader": true,
                    "responsive": true,
                    "scrollY": "80vh",
                    "scrollCollapse": false,
                    "paging": false,
                    "autoWidth": true,
                    "bInfo" : false
                });
            } );
        </script>

</html>
HTML;

    echo $page;
}
