<?php
/* This is the initial index page which is rendered as soon as the site is rendered. It basically displays three options like Do you want to go to the Undergraduate application page, the graduate application page or the administrative portal.  */

require_once('support.php');

$body = <<<HTML
<form action="{$_SERVER['PHP_SELF']}" method="post">

              
  <div class="card-deck">

    <div class="card text-center">
      
      <div class="card-body">

        <h1 class="card-title">Undergraduate Students</h1>
        <p>Application for undergraduate students looking to be TAs</p>

      </div>

        <div class="card-footer">
          <a href="ugPage.php" class="btn btn-lg btn-primary" role="button" name="undergrad" id="undergrad">UG</a>
        </div>

    </div>

    <div class="card text-center">

      <div class="card-body">
      
        <h1 class="card-title">Graduate Students</h1>
        <p>Application for graduate students looking to be TAs</p>

      </div>

        <div class="card-footer">
          <a href="gPage.php" class="btn btn-lg btn-primary" role="button" name="undergrad" id="undergrad">G</a>
        </div>
        
    </div>

    <div class="card text-center">
      
      <div class="card-body">
        <h1 class="card-title">Administrative Portal</h1>
        <p>Faculty and Staff resources for selecting TAs</p>
      </div>

      <div class="card-footer">
        <a href="Admin/" class="btn btn-lg btn-primary" role="button" name="admin" id="admin">A</a>
      </div>

    </div>

</div>

</form>
HTML;

generatePage($body);
