<?php
// This is the application page for the Undergraduate applicant. It is basically a huge form which gets data from the undergraduate student, then creates a Student object which is passed on to the review page, the object contains all the information that was filled in the form. This page is not created using generate page due to various echo statements used inside the actual options. 
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/Student/Student.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/CASlogin/CASlogin.php');

// If the applications are closed then take them to another page
$json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/closed.json'), true);

if ($json['Closed']) {
  echo "<script>window.location = '/closed.php';</script>";
}

$firstname = "";
$lastname = "";
$mi = "";
$phone1 = "";
$phone2 = "";
$phone3 = "";
$directoryid= phpCAS::getUser();

$email = "";
$uid = "";
$gpa = "";

//current semesters completed
$semesters = "";
$department = "";

//optional file upload
$transcript = NULL;
$prevta = "";
$comments = "";


//currenty a TA
$currta = "";

$course1TAed = "";
$course1TAedTook = "";
$course1TAedInstructor = "";

$course2TAed = "";
$course2TAedTook = "";
$course2TAedInstructor = "";

$course3TAed = "";
$course3TAedTook = "";
$course3TAedInstructor = "";

// Choices and its options
$course1 = "";
$course1Took = "";
$course1Grade = "";
$course1TookInstructor = "";

$course2 = "";
$course2Took = "";
$course2Grade = "";
$course2TookInstructor = "";

$course3 = "";
$course3Took = "";
$course3Grade = "";
$course3TookInstructor = "";

$course4 = "";
$course4Took = "";
$course4Grade = "";
$course4TookInstructor = "";

$course5 = "";
$course5Took = "";
$course5Grade = "";
$course5TookInstructor = "";

// text file with list of all CMSC classes
$classes = file('Assets/classes.txt');

/* This is the condition in which all the form answer gathering takes place. First take in all the values from the form, then create the Student object and then pass on to the reviewPage */
if (isset($_POST['submitApp'])) {

  if (!is_string($_POST['firstname'])) {
    $firstname = null;
  } else {
    $firstname = trim($_POST['firstname']);
  }

  $lastname = trim($_POST['lastname']);

  if (isset($_POST['mi'])) {
    $mi = trim($_POST['mi']);
  }

  $phone1 = trim($_POST['phone1']);
  $phone2 = trim($_POST['phone2']);
  $phone3 = trim($_POST['phone3']);

  $email = trim($_POST['email']);
  $uid = trim($_POST['uid']);
  $gpa = trim($_POST['gpa']);
  $semesters = trim($_POST['numSemesters']);
  $department = $_POST['department'];

  if (isset($_FILES['transcript'])) {
    $transcript = $_FILES['transcript']['tmp_name'];
    $fp = @fopen($transcript, "r");
    $transcript = @fread($fp, $_FILES['transcript']['size']);
    @fclose($fp);
  }

  $prevta = $_POST['prevta'];

  $course1TAed = trim($_POST['course1TAed']);
  $course1TAedTook = trim($_POST['course1TAedTook']);
  $course1TAedInstructor = trim($_POST['course1TAedInstructor']);

  $course2TAed = trim($_POST['course2TAed']);
  $course2TAedTook = trim($_POST['course2TAedTook']);
  $course2TAedInstructor = trim($_POST['course2TAedInstructor']);

  $course3TAed = trim($_POST['course3TAed']);
  $course3TAedTook = trim($_POST['course3TAedTook']);
  $course3TAedInstructor = trim($_POST['course3TAedInstructor']);

  $course1 = trim($_POST['course1']);
  $course1Took = trim($_POST['course1Took']);
  $course1Grade = trim($_POST['course1Grade']);
  $course1TookInstructor = trim($_POST['course1TookInstructor']);

  $course2 = trim($_POST['course2']);
  $course2Took = trim($_POST['course2Took']);
  $course2Grade = trim($_POST['course2Grade']);
  $course2TookInstructor = trim($_POST['course2TookInstructor']);

  $course3 = trim($_POST['course3']);
  $course3Took = trim($_POST['course3Took']);
  $course3Grade = trim($_POST['course3Grade']);
  $course3TookInstructor = trim($_POST['course3TookInstructor']);

  $course4 = trim($_POST['course4']);
  $course4Took = trim($_POST['course4Took']);
  $course4Grade = trim($_POST['course4Grade']);
  $course4TookInstructor = trim($_POST['course4TookInstructor']);

  $course5 = trim($_POST['course5']);
  $course5Took = trim($_POST['course5Took']);
  $course5Grade = trim($_POST['course5Grade']);
  $course5TookInstructor = trim($_POST['course5TookInstructor']);

  if (isset($_POST['comments'])) {
    $comments = htmlspecialchars(trim($_POST['comments']));
  }

  $applicant = new Student(
    $firstname,
    $lastname,
    $mi,
    $phone1 . "-" . $phone2 . "-" . $phone3,
    $email,
    $directoryid,
    $uid,
    $gpa,
    $semesters,
    $department,
    $transcript,
    $prevta,
    $course1TAed,
    $course1TAedTook,
    $course1TAedInstructor,
    $course2TAed,
    $course2TAedTook,
    $course2TAedInstructor,
    $course3TAed,
    $course3TAedTook,
    $course3TAedInstructor,
    $course1,
    $course1Took,
    $course1Grade,
    $course1TookInstructor,
    $course2,
    $course2Took,
    $course2Grade,
    $course2TookInstructor,
    $course3,
    $course3Took,
    $course3Grade,
    $course3TookInstructor,
    $course4,
    $course4Took,
    $course4Grade,
    $course4TookInstructor,
    $course5,
    $course5Took,
    $course5Grade,
    $course5TookInstructor,
    $comments
  );

  $_SESSION['applicant'] = $applicant;
  $_SESSION['Student_type'] = "Ugrad";

  header("Location: reviewPage.php");
}
?>

<!DOCTYPE html>
<html>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="Assets/Bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="Assets/Bootstrap/bootstrap.min.js">

  <title>Undergrad TA App</title>
  <style>
    @media (max-width: 623px) {
      h3 {
        font-size: 100%;
      }

      .page-heading {
        font-size: 150%;
      }
    }
  </style>
</head>

<body>
  <script src="https://umd-header.umd.edu/build/bundle.js?search=0&amp;search_domain=&amp;events=0&amp;news=0&amp;schools=0&amp;admissions=0&amp;support=0&amp;support_url=&amp;wrapper=0&amp;sticky=0"></script>
  <noscript><br />
    <div class="um-brand pb-0 mb-0">
      <div id="logo">
        <a href="cs.umd.edu" target="_blank">University of Maryland</a>
      </div>
    </div>
  </noscript>

  <nav class="navbar navbar-dark shadow-sm p-3 mb-5 rounded" style="background-color: #e8e8e8">
    <img class="navbar-brand img-fluid col-5" src="Assets/logo.png">
    <h3 class="text-fluid text-right">Teaching Assistant Application System</h3>
  </nav>


  <h1 class="container text-center display-4 page-heading">Undergraduate Application</h1>

  <div class="container">

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">

      <div class="card mt-4">

        <div class="card-header">
          Student Information
        </div>

        <div class="card-body">

          <div class="row">

            <div class="col-5">
              <label for="firstname">First Name</label>
              <input class="form-control" type="text" name="firstname" required="true">
            </div>

            <div class="col-2">
              <label for="mi">M.I.</label>
              <input class="form-control" type="text" name="mi">
            </div>

            <div class="col-5">
              <label for="lastname">Last Name</label>
              <input class="form-control" type="text" name="lastname" required="true">
            </div>

          </div>

          <div class="row">

            <div class="col-lg-6">
              <label for="phone">Phone</label>
              <div class="row mx-1">
                <input class="form-control col-2" type="tel" pattern="\d{3}" placeholder="###" name="phone1" maxlength="3" required="true">-
                <input class="form-control col-2" type="tel" pattern="\d{3}" placeholder="###" name="phone2" maxlength="3" required="true">-
                <input class="form-control col-2" type="tel" pattern="\d{4}" placeholder="####" name="phone3" maxlength="4" required="true">
              </div>
            </div>

            <div class="col-lg-6">
              <label for="email">Email</label>
              <input class="form-control" type="email" name="email" required="true">
            </div>

          </div>

          <div class="row">

            <div class="col-4">
              <label for="uid">UID #</label>
              <input class="form-control" type="text" pattern="[0-9]{9}" placeholder="#########" maxlength="9" name="uid" required="true">
            </div>

            <div class="col-4">
              <label for="gpa">Cumulative GPA</label>
              <input class="form-control" type="text" pattern="[0-4][\.][0-9]{2}" placeholder="#.##" maxlength="4"  name="gpa" required="true">
            </div>

            <div class="col-4">
              <label for="numSemesters">Completed Semesters at UMD</label>
              <input class="form-control" type="text" name="numSemesters" placeholder="#" required="true">
            </div>

          </div>

          <div class="row mt-3">

            <div class="col">
              <label>Department</label>
              <select class="form-control d-block w-50" name="department" required="true">
                <option value="CS">Computer Science</option>
                <option value="CE">Computer Engineering</option>
                <option value="EE">Electrical Engineering</option>
                <option value="OTHER">Other</option>
              </select>
              <label for="otherDepartment">Please tell the Department if you have selected "Other" above:</label>
              <input type="text" name="otherDepartment">
            </div>

            <div class="col">
              <label class="d-block">
                Upload Unofficial Transcript (.pdf only)
                <input class="d-block" name="transcript" type="file" accept=".pdf" required="true">
              </label>
            </div>
          </div>
        </div>

      </div>

      <div class="card my-4">

        <div class="card-header">
          TA Information
        </div>

        <div class="card-body">

          <div class="form-group">
            <label for="prevta">I have been a CMSC TA before:</label>
            <input type="radio" name="prevta" id="prevta" value="yes" required="true">Yes
            <input type="radio" name="prevta" id="prevta" value="no">No
          </div>

          <div class="form-group">
            <label class="d-block text-center">If yes, I have TAed for the following classes (top 3 most recent, if applicable):</label>
            <div class="row">
              <div class="col">
                <label>1st Class</label><br>
                <select class="form-control" name="course1TAed">
                  <option value="" selected>--</option>
                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>When you TAed for it:</label><br>
                <select class="form-control" name="course1TAedTook">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor for whom you TAed</label>
                <input class="form-control" type="text" name="course1TAedInstructor">
              </div>

            </div>

          </div>

          <div class="form-group">

            <div class="row">
              <div class="col">
                <label>2nd Class</label><br>
                <select class="form-control" name="course2TAed">
                  <option value="" selected>--</option>

                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>When you TAed for it:</label><br>
                <select class="form-control" name="course2TAedTook">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor for whom you TAed</label>
                <input class="form-control" type="text" name="course2TAedInstructor">
              </div>

            </div>

          </div>

          <div class="form-group">

            <div class="row">
              <div class="col">
                <label>3rd Class</label><br>
                <select class="form-control" name="course3TAed">
                  <option value="" selected>--</option>

                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>When you TAed for it:</label><br>
                <select class="form-control" name="course3TAedTook">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor for whom you TAed</label>
                <input class="form-control" type="text" name="course3TAedInstructor">
              </div>

            </div>
          </div>

          <div class="form-group">
            <label for="priorTAexp" class="control-label">Prior TA experience when not at UMD</label>
            <div class="col-sm-9">
              <textarea rows="5" class="form-control" name="priorTAexp"></textarea>
            </div>
          </div>

          <div class="form-group my-3">
            <label class="d-block text-center">List the top 5 courses you would like to TA:</label>
            <div class="row">
              <div class="col">
                <label>1st Choice</label>
                <select class="form-control" name="course1" required="true">
                  <option value="" selected>--</option>
                  <?php
                  $cmscUgradClasses = preg_grep(("/CMSC[1-4][0-9][0-9]|CMSC[1-4][0-9][0-9][A-Z]/"), $classes);
                  foreach ($cmscUgradClasses as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Grade</label><br>
                <select class="form-control" name="course1Grade" required="true">
                  <option value="A+">A+</option>
                  <option value="A">A</option>
                  <option value="A-">A-</option>
                  <option value="B+">B+</option>
                  <option value="B">B</option>
                  <option value="B-">B-</option>
                  <option value="C+">C+</option>
                  <option value="C">C</option>
                  <option value="C-">C-</option>
                  <option value="D+">D+</option>
                  <option value="D">D</option>
                  <option value="D-">D-</option>
                  <option value="F">F</option>
                </select>
              </div>

              <div class="col">
                <label>When you TOOK it:</label><br>
                <select class="form-control" name="course1Took" required="true">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor</label>
                <input class="form-control" type="text" name="course1TookInstructor">
              </div>

            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col">
                <label>2nd Choice</label>
                <select class="form-control" name="course2">
                  <option value="" selected>--</option>

                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Grade</label><br>
                <select class="form-control" name="course2Grade" required="true">
                  <option value="A+">A+</option>
                  <option value="A">A</option>
                  <option value="A-">A-</option>
                  <option value="B+">B+</option>
                  <option value="B">B</option>
                  <option value="B-">B-</option>
                  <option value="C+">C+</option>
                  <option value="C">C</option>
                  <option value="C-">C-</option>
                  <option value="D+">D+</option>
                  <option value="D">D</option>
                  <option value="D-">D-</option>
                  <option value="F">F</option>
                </select>
              </div>

              <div class="col">
                <label>When you TOOK it:</label><br>
                <select class="form-control" name="course2Took" required="true">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor</label>
                <input class="form-control" type="text" name="course2TookInstructor">
              </div>

            </div>
          </div>

          <div class="form-group">

            <div class="row">
              <div class="col">
                <label>3rd Choice</label>
                <select class="form-control" name="course3">
                  <option value="" selected>--</option>

                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Grade</label><br>
                <select class="form-control" name="course3Grade" required="true">
                  <option value="A+">A+</option>
                  <option value="A">A</option>
                  <option value="A-">A-</option>
                  <option value="B+">B+</option>
                  <option value="B">B</option>
                  <option value="B-">B-</option>
                  <option value="C+">C+</option>
                  <option value="C">C</option>
                  <option value="C-">C-</option>
                  <option value="D+">D+</option>
                  <option value="D">D</option>
                  <option value="D-">D-</option>
                  <option value="F">F</option>
                </select>
              </div>

              <div class="col">
                <label>When you TOOK it:</label><br>
                <select class="form-control" name="course3Took" required="true">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor</label>
                <input class="form-control" type="text" name="course3TookInstructor">
              </div>

            </div>
          </div>

          <div class="form-group">

            <div class="row">
              <div class="col">
                <label>4th Choice</label>
                <select class="form-control" name="course4">
                  <option value="" selected>--</option>

                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Grade</label><br>
                <select class="form-control" name="course4Grade" required="true">
                  <option value="A+">A+</option>
                  <option value="A">A</option>
                  <option value="A-">A-</option>
                  <option value="B+">B+</option>
                  <option value="B">B</option>
                  <option value="B-">B-</option>
                  <option value="C+">C+</option>
                  <option value="C">C</option>
                  <option value="C-">C-</option>
                  <option value="D+">D+</option>
                  <option value="D">D</option>
                  <option value="D-">D-</option>
                  <option value="F">F</option>
                </select>
              </div>

              <div class="col">
                <label>When you TOOK it:</label><br>
                <select class="form-control" name="course4Took" required="true">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor</label>
                <input class="form-control" type="text" name="course4TookInstructor">
              </div>

            </div>
          </div>

          <div class="form-group">

            <div class="row">
              <div class="col">
                <label>5th Choice</label>
                <select class="form-control" name="course5">
                  <option value="" selected>--</option>

                  <?php
                  foreach ($classes as $class) {
                    echo '<option value="' . $class . '">' . $class . '</option>';
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Grade</label><br>
                <select class="form-control" name="course5Grade" required="true">
                  <option value="A+">A+</option>
                  <option value="A">A</option>
                  <option value="A-">A-</option>
                  <option value="B+">B+</option>
                  <option value="B">B</option>
                  <option value="B-">B-</option>
                  <option value="C+">C+</option>
                  <option value="C">C</option>
                  <option value="C-">C-</option>
                  <option value="D+">D+</option>
                  <option value="D">D</option>
                  <option value="D-">D-</option>
                  <option value="F">F</option>
                </select>
              </div>

              <div class="col">
                <label>When you TOOK it:</label><br>
                <select class="form-control" name="course5Took" required="true">
                  <?php
                  $semNames = array("Fall", "Summer", "Spring");
                  for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                    for ($x = 0; $x < count($semNames); $x++) {
                      echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                    }
                    echo "<br>";
                  }
                  ?>
                </select>
              </div>

              <div class="col">
                <label>Instructor</label>
                <input class="form-control" type="text" name="course5TookInstructor">
              </div>

            </div>
          </div>



          <label for="comments" class="control-label col-sm-3">Additional Comments</label>
          <div class="col-sm-9">
            <textarea rows="5" class="form-control" name="comments"></textarea>
          </div>
        </div>
      </div>
  </div>

  <div class="form-group">

    <blockquote class="blockquote text-center">
      <p class="text-center"><strong>By checking the box below, you agree that you: </strong></p>
      <p>1. Are eligible to TA for the classes you have selected.</p>
      <p>2. Understand that you may not hear back about a TA position by the end of the semester.</p>
      <p>3. Will not e-mail the faculty member/instructor directly regarding TA positions.</p>
    </blockquote>

    <div class="form-check form-control-lg text-center">
      <label class="form-check-label">
        <input type="checkbox" name="waiver" required="true"> <mark>I agree.</mark>
      </label>
    </div>

  </div>

  <div class="form-group text-center">
    <button class="btn btn-lg btn-primary" type="submit" name="submitApp">Submit Application</button>
  </div>

  </form>

  </div>

  <footer class="page-footer font-small mt-4 pt-2" style="background-color: #e8e8e8">

    <div class="container-fluid text-center">
      <a href="https://cs.umd.edu">UMD CS</a>
    </div>

    <div class="footer-copyright text-center py-3">@ <?php echo date ("Y");?> Copyright:
      <a href="https://www.umd.edu">University of Maryland</a>
    </div>

  </footer>

</body>



</html>