<?php
/* This is page which will show if the application is closed*/

require_once('support.php');

$body = "<p class='text-center'>Applications are not currently open.</p>";

generatePage($body,"Application Closed","Sorry");
