# TAAPP
![Index Page](/Assets/Index-ScreenShot.png)
This is the a repository which is used to implement a web application which is used for potential students to apply for the position of the TA in the computer Science Department of University of Maryland, College Park. The project now uses composer to add some libraries to make stuff easier to do. See composer.json for the libraries being used. 


## Authors

* **Apoorv Mittal** - *Initial work* - [Apoorv-Mittal](https://github.com/Apoorv-Mittal)

See also the list of [contributors](https://github.com/Apoorv-Mittal/TAAPP/graphs/contributors) who participated in this project.

## Acknowledgments

* [Nelson Padua-Perez](https://github.com/nelson-padua-perez)


## Future improvements

* Faculty wishes and then Tom approves
* Darker borders on tables 