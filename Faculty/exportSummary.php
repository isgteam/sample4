<?php
/* This file is used to dump all the data from the ugrad table to an excel file.   */
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

// Get Instructor from the URL
$instructor = $_GET['Instructor'];

// Get class from URL
$class=  $_GET['class'];

$sem = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/semester.json'), true);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$data = $database->select("Ugrad", ["FirstName", "LastName", "Phone", "Email", "DirectoryId", "UID", "gradStatus"],
    [
        "OR" => [
            "AND #Key 1" => [
                "assignedClass" => $class,
                "assignedInstructor" => $instructor,
            ],
            "AND #Key 2" => [
                "fixedClass" => $class,
                "fixedInstructor" => $instructor,
            ]
        ]
    ] 
);

$data = array_merge($data, $database->select("Grad", ["FirstName", "LastName", "Phone", "Email", "DirectoryId", "UID", "gradStatus"],
[
    "OR" => [
        "AND #Key 1" => [
            "assignedClass" => $class,
            "assignedInstructor" => $instructor,
        ],
        "AND #Key 2" => [
            "fixedClass" => $class,
            "fixedInstructor" => $instructor,
        ]
    ]
] 
));

if ($data == null) {
    header("Location: /Faculty/");
}

$headings = array();

foreach(array_keys($data[0]) as $k){
    array_push($headings, $tableDisplay[$k]);
}

$arrayData = [
    array_values($headings)
];

foreach ($data as $row) {
    array_push($arrayData, $row);
}

$sheet->fromArray(
    $arrayData,
    'A1'
);

// Auto Sizing the columns in the excel sheet
for ($cols = 0; $cols < count($arrayData[0]); $cols++) {
    $temp = chr($cols + 65);
    if (($cols + 65) > 90)
        $temp = 'A' . chr($cols%26 + 65);
    if($cols/26 >= 2)
        $temp = 'B' . chr($cols%26 + 65);
    $sheet->getColumnDimension($temp)->setAutoSize(true);
}

// Save the file directly to php output
$writer = new Xls($spreadsheet);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="SummaryAssignedTAsfor'.$_GET['class'].'-'.$sem['semester'].'.xls"');
header("Pragma: no-cache");
header("Expires: 0");
$writer->save("php://output");
exit;
