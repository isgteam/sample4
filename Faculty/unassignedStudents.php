<?php
/* This page shows all the unassigned students in the DB. This will mostly comprises of any student who has not been manually assigned or has not been assigned by the automatic system. It basically makes 2 tables one for graduate and one for undergraduate  */

require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

$body =  '
<a href="Export/ugExport.php"><button type="button" class="btn btn-success">Export Unassigned Undergraduates</button></a>
<a href="Export/gExport.php"><button type="button" class="btn btn-success">Export Unassigned Graduates</button></a>
<div class="table-responsive">
<table id="dataTable" class="table table-bordered table-striped table-sm" >  
<thead>
    <tr>  
';

if (($key = array_search('assignedClass', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('assignedInstructor', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('fixedClass', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}
if (($key = array_search('fixedInstructor', $fieldsToDisplay)) !== false) {
    unset($fieldsToDisplay[$key]);
}

$fieldsToDisplay[]= "GPA";
$fieldsToDisplay[]= "course1TAed";
$fieldsToDisplay[]= "Email";
$fieldsToDisplay[]= "Transcript";
$fieldsToDisplay[]= "Choice_1";
$fieldsToDisplay[]= "Choice_2";
$fieldsToDisplay[]= "Choice_3";


foreach ($fieldsToDisplay as $rec) {
    $body .= '<th scope="col">' . $tableDisplay[$rec] . '</th>';
}

$body .= '</tr>
</thead>
<tbody>';

$body .= makeTable("Ugrad") . makeTable("Grad");

$body .= '</tbody></table></div>';


$breadcrumb = array("Home" => "../", "Faculty" => "index.php");

$totalApplicants = $database->count("Ugrad", [
    "AND" => [
        "assignedClass[=]" => null,
        "OR" => [
            "fixedClass[=]" => "", 
            "fixedClass" => null
        ]
    ]
]) + 
$database->count("Grad", [
    "AND" => [
        "assignedClass[=]" => null,
        "OR" => [
            "fixedClass[=]" => "", 
            "fixedClass" => null
        ]
    ]
]);

generatePage($body, "Administrative Portal-Unassigned Applicants", "Unassigned Applicants(".$totalApplicants.")", $breadcrumb);

function makeTable($tableName)
{
    global $fieldsToDisplay;
    global $database;

    $body ="";

    // Third element in the display field should always be the UID as that is the key in the DB.
    if (!in_array("UID" , $fieldsToDisplay)){
        array_splice( $fieldsToDisplay, 2, 0, "UID" );
    }

    $result = $database->select($tableName, $fieldsToDisplay, [
        "AND" => [
            "assignedClass[=]" => null,
            "OR" => [
                "fixedClass[=]" => "", 
                "fixedClass" => null
            ]
        ]
    ]);

    // If no one is unassigned then just return empty string
    if (count($result) == 0)
        return '';

        foreach ($result as $row) { 
            $body .= '<tr>';  
            foreach ($row as $key=>$value) { 
                if( !$value ) 
                    $value = " ";
                // If you have to display transcript display a link
                if ( strcmp("Transcript",$key) == 0 && strcmp($value," ") != 0){
                    $body .= '<td><a target="_blank" href="../Admin/TranscriptPdf.php?UID=' . $row['UID'] . '&type='.$tableName.'">Transcript</a></td>' ;
                }
                else{
                    $body .= "<td>".$value."</td>" ;  
                }
    
            }
        }

    return $body;
}
