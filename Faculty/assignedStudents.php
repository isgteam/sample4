<?php
/* This page shows all the unassigned students in the DB. This will mostly comprises of any student who has not been manually assigned or has not been assigned by the automatic system. It basically makes 2 tables one for graduate and one for undergraduate  */

session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

$instructorName = $_SESSION["instructorName"];

$body =  '
<div class="table-responsive">
<table id="dataTable" class="table table-bordered table-striped table-sm" >  
<thead>
    <tr>  
';

if (($key = array_search('UID', $fieldsToDisplay)) !== false) {
    array_splice( $fieldsToDisplay, $key+1, 0, array("Email","Transcript", "GPA", "course1TAed", "course1TAedInstructor" ) );
}

foreach ($fieldsToDisplay as $rec) {
    $body .= '<th scope="col">' . $tableDisplay[$rec] . '</th>';
}

$body .= '</tr>
</thead>
<tbody>';

$body .= makeTable("Ugrad") . makeTable("Grad");

$body .= '</tbody></table></div>';

$breadcrumb = array("Home" => "../", "Faculty" => "index.php");

$totalApplicants = $database->count("Ugrad", [
    "OR" => [
        "assignedInstructor" => $instructorName,
        "fixedInstructor" => $instructorName
    ]
]) + 
$database->count("Grad", [
    "OR" => [
        "assignedInstructor" => $instructorName,
        "fixedInstructor" => $instructorName
    ]
]);

generatePage($body, "Administrative Portal-Assigned Applicants", "Assigned Applicants(".$totalApplicants.")", $breadcrumb);

function makeTable($tableName)
{
    global $fieldsToDisplay, $database, $instructorName;

    $body ="";

    // Third element in the display field should always be the UID as that is the key in the DB.
    if (!in_array("UID" , $fieldsToDisplay)){
        array_splice( $fieldsToDisplay, 2, 0, "UID" );
    }

    $result = $database->select($tableName, $fieldsToDisplay, [
        "OR" => [
            "assignedInstructor" => $instructorName,
            "fixedInstructor" => $instructorName
		]
    ]);

    // If no one is unassigned then just return empty string
    if (count($result) == 0)
        return '';

        foreach ($result as $row) { 
            $body .= '<tr>';  
            $class= NULL;
            $ins = NULL;
            foreach ($row as $key=>$value) { 
                if( !$value ) {
                    $value = " ";
                }
                // If you have to display transcript display a link
                if ( strcmp("Transcript",$key) == 0 && strcmp($value," ") != 0){
                    $body .= '<td><a target="_blank" href="../Admin/TranscriptPdf.php?UID=' . $row['UID'] . '&type='.$tableName.'">Transcript</a></td>' ;
                }
                else{
                    $body .= "<td>".$value."</td>" ;  
                }
    
            }
        }

    return $body;
}
