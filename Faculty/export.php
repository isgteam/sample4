<?php
/* This file is used to dump all the data from the ugrad table to an excel file.   */
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/filter.php');

// Get Instructor from the URL
$instructor = $_GET['Instructor'];

// Get class from URL
$class=  $_GET['class'];

$sem = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/semester.json'), true);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

$data = $database->select("Ugrad", "*",
    [
        "OR" => [
            "AND #Key 1" => [
                "assignedClass" => $class,
                "assignedInstructor" => $instructor,
            ],
            "AND #Key 2" => [
                "fixedClass" => $class,
                "fixedInstructor" => $instructor,
            ]
        ]
    ] 
);

$arrayData = [];

$transcriptLinks = [];

foreach ($data as $row) {
    $row['Transcript'] = 'Transcript';
    array_push($transcriptLinks, "http://taapp.cs.umd.edu/Admin/TranscriptPdf.php?UID=" . $row['UID'] . "&type=Ugrad");
    array_push($arrayData, $row);
}
$colTrans = "L";

if($data == null){
    $gradCols  = ["*"];
    $colTrans = "K";
}
else {
    $gradCols = array_keys($data[0]);
    $colTrans = "L";
}

$headings = array();

foreach (array_keys($data[0]) as $k) {
    array_push($headings, $tableDisplay[$k]);
}

array_unshift($arrayData, array_values($headings));

$gradCols = array(
    "Date", "FirstName", "LastName", "Phone", "Email", "DirectoryId", "UID", "GPA", "Semesters", "Department", "gradStatus", "Transcript", "course1TAed", "course1TAedTook", "course1TAedInstructor", "course2TAed", "course2TAedTook", "course2TAedInstructor", "course3TAed", "course3TAedTook", "course3TAedInstructor",
    "Choice_1", "Choice_2", "Choice_3", "Choice_4", "Choice_5", "Comments", "assignedClass", "assignedInstructor", "fixedClass", "fixedInstructor", "Date_Assigned", "ADMIN_NOTES"
);

// Get the grad data
$data = $database->select("Grad", $gradCols ,
[
    "OR" => [
        "AND #Key 1" => [
            "assignedClass" => $class,
            "assignedInstructor" => $instructor,
        ],
        "AND #Key 2" => [
            "fixedClass" => $class,
            "fixedInstructor" => $instructor,
        ]
    ]
] 
);

if ($data == null && count($arrayData) == 0) {
    header("Location: /Faculty/");
}


if ($data != null) {

    foreach ($data as $row) {
        // 14 empty
        $row['Transcript'] = 'Transcript';
        array_push($transcriptLinks, "http://taapp.cs.umd.edu/Admin/TranscriptPdf.php?UID=" . $row['UID'] . "&type=Grad");

        // Fill empty space
        $noColsFound = array_fill(0, 15, "");
        array_splice($row, 21, 0, $noColsFound);

        array_push($arrayData, $row);
    }
}

$sheet->fromArray(
    $arrayData,
    'A1'
);

// Actually hyperlinking the transcript
for ($rows = 0; $rows < count($transcriptLinks); $rows++) {
    $sheet->getCell($colTrans . ($rows + 2))->getHyperlink()->setUrl($transcriptLinks[$rows]);
    $sheet->getStyle($colTrans. ($rows + 2))->applyFromArray(array('font' => array('color' => ['rgb' => '0000FF'], 'underline' => 'single')));
}

// Auto Sizing the columns in the excel sheet
for ($cols = 0; $cols < count($arrayData[0]); $cols++) {
    $temp = chr($cols + 65);
    if (($cols + 65) > 90)
        $temp = 'A' . chr($cols%26 + 65);
    if($cols/26 >= 2)
        $temp = 'B' . chr($cols%26 + 65);
    $sheet->getColumnDimension($temp)->setAutoSize(true);
}

// Save the file directly to php output
$writer = new Xls($spreadsheet);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="AssignedTAsfor'.$_GET['class'].'-'.$sem['semester'].'.xls"');
header("Pragma: no-cache");
header("Expires: 0");
$writer->save("php://output");
exit;
