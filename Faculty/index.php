<?php
/* This is the index page for the faculty to login and then view their TAs and everything. 
 */

session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/support.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/AccessDB/DBquery.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Admin/Assign/config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/Assets/CASlogin/CASlogin.php');

$info = phpCAS::getAttributes();

$instructor = $database->select("Instructors","Name", [
	"DirectoryId" => phpCAS::getUser()
]);

$instructorName = $instructor[0];

$_SESSION["instructorName"]=$instructorName;

if (!$database->has("Courses", ["Instructor" => $instructorName])) {
	echo "<script>alert('You are not supposed to be here. I am taking you back.');window.location = '/';</script>";
}

// Creating an associative array for links in breadcrumbs
$breadcrumb = array("Home" => "../", "Faculty" => ".");

$courses = $database->select("Courses", "*", [
	"Instructor" => $instructorName
]);
	
$numberOfCourses = count($courses);
$numberOfCoursesWithHigherRatio =0 ;
$numberOfAssignedUgrad = $database->count("Ugrad", [
	"OR" => [
		"assignedInstructor" => $instructorName,
		"fixedInstructor" => $instructorName,
	]	
]);
$numberOfAssignedMaster=$database->count("Grad", [
	"AND" => [
		"OR" => [
			"assignedInstructor" => $instructorName,
			"fixedInstructor" => $instructorName,
		],
		"gradStatus" => "MASTERS"
	]
]);

$numberOfAssignedPhd = $database->count("Grad", [
	"AND" => [
		"OR" => [
			"assignedInstructor" => $instructorName,
			"fixedInstructor" => $instructorName,
		],
		"gradStatus" => "PHD"
	]	
]);

$sem = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/semester.json'), true);

$body = '';

foreach ($courses as $course) {

	// If the course is a graduate course
	if (strcmp($course["Name"], "CMSC5") > 0) {
		$body .= graduateClassShow($course);
	} else {
		if ( !isRatioRespected($course) )
			$numberOfCoursesWithHigherRatio += 1;
		$body .= undergraduateClassShow($course);
	}
}

$body .= '</tbody></table>';

$bodyStart =  <<<HTML
<br>
	<div class="card text-center" >
		<div class="card-body">
			<h4 class="card-title">Dashboard</h4>
			<table class="table card-text table-sm">
			<tr>
				<td>Total Courses: $numberOfCourses </td>
				<td>Courses with higher ratio: $numberOfCoursesWithHigherRatio (Labelled yellow)</td>
				<td></td>
			</tr>
			<tr>
				<td>Number of Undergraduate TAs assigned: $numberOfAssignedUgrad</td>
				<td>Number of Master TAs assigned: $numberOfAssignedMaster</td>
				<td>Number of PhD TAs assigned: $numberOfAssignedPhd</td>
			</tr>
			</table>
		</div>
	</div>
	<br>
	<a href="unassignedStudents.php"><button type="button" class="btn btn-success">See Unassigned TA Applicants</button></a>
	<a href="/Faculty/exportSummaryAll.php" style="float:right"><button type="button" class="btn btn-success ml-1">Export All Assigned TAs Summary</button></a>
	<a href="/Faculty/assignedStudents.php" style="float:right"><button type="button" class="btn btn-success">See Assigned TA Applicants</button></a>
	<br>
	<br>
	<table id="dataTable" class="table table-bordered table-striped" >  
	<thead>
		<tr>  
			<th scope="col">Course</th>    
			<th scope="col">Total TAs</th>
			<th scope="col">Grad TAs</th>
			<th scope="col">UnderGrad TAs</th>
			<th scope="col">TA Hours Left</th>
			<th scope="col">Max TA Hours</th>
			<th scope="col">Export to Excel Summary</th>
			<th scope="col">Export to Excel</th>
		</tr>
	</thead>
	<tbody>
HTML;

$body = $bodyStart . $body;

generatePage($body, "Faculty Portal", "Assignments for " . $instructorName. " ". $sem['semester'], $breadcrumb = $breadcrumb);


// Functions
function  graduateClassShow($course)
{
	global $hoursPerGradTA, $database;

	$hours = $course["Hours"] + $course["Additional_Hours"];

	$gradsFull = $database->count("Grad", [
		"OR #a" => [
			"AND #key 1" => [
				"assignedClass" => $course['Name'],
				"assignedInstructor" => $course['Instructor'],
				"HALF_TIME" => 0
			],
			"AND #key 2" => [
				"fixedClass" => $course['Name'],
				"fixedInstructor" => $course['Instructor'],
				"HALF_TIME" => 0
			]
		]
	]);

	$gradsHalf = $database->count("Grad", [
		"OR #a" => [
			"AND #key 1" => [
				"assignedClass" => $course['Name'],
				"assignedInstructor" => $course['Instructor'],
				"HALF_TIME" => 1
			],
			"AND #key 2" => [
				"fixedClass" => $course['Name'],
				"fixedInstructor" => $course['Instructor'],
				"HALF_TIME" => 1
			]
		]
	]);

	$body = '<tr>';
	$body .= "<td>" . $course["Name"] . "</td>";
	$body .= "<td>" . ($gradsFull + $gradsHalf) . "</td>";
	$body .= "<td>" . ($gradsFull + $gradsHalf) . "</td>";
	$body .= "<td>N/A</td>";
	$body .= "<td>" .preg_replace('/^(\d+)$/',"+$1", ($hours - ($hoursPerGradTA * $gradsFull + $hoursPerGradTA/2 * $gradsHalf))*-1 ). "</td>";
	$body .= "<td>" . $hours . "</td>";
	$body .= "<td>
			<a href='/Faculty/exportSummary.php?Instructor=" . $course['Instructor'] . "&class=" . $course['Name'] . "'>Export Summary</a>
		</td>";
	$body .= "<td>
			<a href='/Faculty/export.php?Instructor=" . $course['Instructor'] . "&class=" . $course['Name'] . "'>Export Data</a>
		</td>";
	$body .= '</tr>';

	return $body;
}

function undergraduateClassShow($course)
{
	global $hoursPerUgradTA, $hoursPerGradTA, $database;

	$hours = $course["Hours"] + $course["Additional_Hours"];

	$gradsFull = $database->count("Grad", [
		"OR #a" => [
			"AND #key 1" => [
				"assignedClass" => $course['Name'],
				"assignedInstructor" => $course['Instructor'],
				"HALF_TIME" => 0
			],
			"AND #key 2" => [
				"fixedClass" => $course['Name'],
				"fixedInstructor" => $course['Instructor'],
				"HALF_TIME" => 0
			]
		]
	]);
	$gradsHalf = $database->count("Grad", [
		"OR #a" => [
			"AND #key 1" => [
				"assignedClass" => $course['Name'],
				"assignedInstructor" => $course['Instructor'],
				"HALF_TIME" => 1
			],
			"AND #key 2" => [
				"fixedClass" => $course['Name'],
				"fixedInstructor" => $course['Instructor'],
				"HALF_TIME" => 1
			]
		]
	]);

	$ugradsFull = $database->count("Ugrad", "UID", [
		"OR #a" => [
			"AND #key 1" => [
				"assignedClass" => $course['Name'],
				"assignedInstructor" => $course['Instructor'],
				"HALF_TIME" => 0
			],
			"AND #key 2" => [
				"fixedClass" => $course['Name'],
				"fixedInstructor" => $course['Instructor'],
				"HALF_TIME" => 0
			]
		]	
	]);

	$ugradsHalf = $database->count("Ugrad", "UID", [
		"OR #a" => [
			"AND #key 1" => [
				"assignedClass" => $course['Name'],
				"assignedInstructor" => $course['Instructor'],
				"HALF_TIME" => 1
			],
			"AND #key 2" => [
				"fixedClass" => $course['Name'],
				"fixedInstructor" => $course['Instructor'],
				"HALF_TIME" => 1
			]
		]	
	]);

	$body = '<tr class="'.returnWarning(!isRatioRespected($course)).'">';
	$body .= "<td>" . $course["Name"] . "</td>";
	$body .= "<td>" . (($ugradsFull + $ugradsHalf) + ($gradsFull + $gradsHalf)) . "</td>";
	$body .= "<td>" . ($gradsFull + $gradsHalf) . "</td>";
	$body .= "<td>" . ($ugradsFull + $ugradsHalf) . "</td>";
	$body .= "<td>" . preg_replace('/^(\d+)$/',"+$1",($hours - ($hoursPerUgradTA * $ugradsFull + $hoursPerGradTA * $gradsFull + $hoursPerUgradTA/2 * $ugradsHalf + $hoursPerGradTA/2 * $gradsHalf))*-1) . "</td>";
	$body .= "<td>" . $hours . "</td>";
	$body .= "<td>
			<a href='/Faculty/exportSummary.php?Instructor=" . $course['Instructor'] . "&class=" . $course['Name'] . "'>Export Summary</a>
		</td>";
	$body .= "<td>
			<a href='/Faculty/export.php?Instructor=" . $course['Instructor'] . "&class=" . $course['Name'] . "'>Export Data</a>
		</td>";
	$body .= '</tr>';

	return $body;
}

function returnWarning($cond){
    if($cond)
        return "table-warning";
    return "";
}