<?php
// This is the application page for the Gradute applicant. It is basically a huge form which gets data from the graduate student, then creates a gradStudent object which is passed on to the review page, the object contains all the information that was filled in the form. This page is not created using generate page due to various echo statements used inside the actual options.
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/Student/gradStudent.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Assets/CASlogin/CASlogin.php');

// If the applications are closed then take them to another page
$json = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/Assets/closed.json'), true);

if ($json['Closed']) {
    echo "<script>window.location = '/closed.php';</script>";
}

$firstname = "";
$lastname = "";
$mi = "";
$email = "";
$uid = "";
$gpa = "";
$directoryid= phpCAS::getUser();

//current semesters completed
$semesters = "";
$department = "";

//optional file upload
$transcript = null;
$prevta = "";
$comments = "";

//phd or ms
$gradstatus = "";
$masters = "";

//entry semester
$semester = "";

//entry year
$year = "";
$advisor = "";

//currenty a TA
$currta = "";

//graduate step
$step = "";

//current course TAing for
$course = "";
//instructor of course
$instructor = "";

//Previous TA exp
$course1TAed = "";
$course1TAedOverlap = "";
$course1TAedGrade = "";
$course1TAedTook = "";
$course1TAedInstructor = "";

$course2TAed = "";
$course2TAedOverlap = "";
$course2TAedGrade = "";
$course2TAedTook = "";
$course2TAedInstructor = "";


$course3TAed = "";
$course3TAedOverlap = "";
$course3TAedGrade = "";
$course3TAedTook = "";
$course3TAedInstructor = "";

//passed MEI Eval
$mei = "";

//currently taking UMEI course
$umei = "";
$position = "";
$preferredsemester = "";
$preferredyear = "";
$course1 = "";
$course2 = "";
$course3 = "";
$course4 = "";
$course5 = "";

// text file with list of all CMSC classes
$classes = file('Assets/classes.txt');

/* This is the condition in which all the form answer gathering takes place. First take in all the values from the form, then create the gradStudent object and then pass on to the reviewPage */
if (isset($_POST['submitApp'])) {
    $firstname = trim($_POST['firstname']);
    $lastname = trim($_POST['lastname']);

    if (isset($_POST['mi'])) {
        $mi = trim($_POST['mi']);
    }

    $phone1 = trim($_POST['phone1']);
    $phone2 = trim($_POST['phone2']);
    $phone3 = trim($_POST['phone3']);

    $email = trim($_POST['email']);
    $uid = trim($_POST['uid']);
    $gpa = trim($_POST['gpa']);
    $semesters = trim($_POST['numSemesters']);
    $department = $_POST['department'];

    if (isset($_FILES['transcript'])) {
        $transcript = $_FILES['transcript']['tmp_name'];
        $fp = @fopen($transcript, "r");
        $transcript = @fread($fp, $_FILES['transcript']['size']);
        @fclose($fp);
    }

    $prevta = trim($_POST['prevta']);

    if (isset($_POST['comments'])) {
        $comments = htmlspecialchars(trim($_POST['comments']));
    }

    $gradstatus = trim($_POST['gradstatus']);
    $masters = trim($_POST['masters']);
    $semester = trim($_POST['semester']);
    $year = trim($_POST['year']);
    $advisor =  trim($_POST['advisor']);
    $currta = trim($_POST['currta']);
    $step = trim($_POST['step']);
    $course = trim($_POST['course']);
    $instructor = trim($_POST['instructor']);

    $course1TAed = trim($_POST['Course1TAed']);
    $course1TAedOverlap = trim($_POST['Course1TAedOverlap']);
    $course1TAedGrade = trim($_POST['Course1TAedGrade']);
    $course1TAedTook = trim($_POST['Course1TAedTook']);
    $course1TAedInstructor = trim($_POST['Course1TAedInstructor']);
    $course2TAed = trim($_POST['Course2TAed']);
    $course2TAedOverlap = trim($_POST['Course2TAedOverlap']);
    $course2TAedGrade = trim($_POST['Course2TAedGrade']);
    $course2TAedTook = trim($_POST['Course2TAedTook']);
    $course2TAedInstructor = trim($_POST['Course2TAedInstructor']);
    $course3TAed = trim($_POST['Course3TAed']);
    $course3TAedOverlap = trim($_POST['Course3TAedOverlap']);
    $course3TAedGrade = trim($_POST['Course3TAedGrade']);
    $course3TAedTook = trim($_POST['Course3TAedTook']);
    $course3TAedInstructor = trim($_POST['Course3TAedInstructor']);

    $mei = trim($_POST['mei']);
    $umei = trim($_POST['umei']);
    $position = trim($_POST['position']);
    $preferredsemester = trim($_POST['preferredsemester']);
    $preferredyear = trim($_POST['preferredyear']);
    $course1 = trim($_POST['course1']);
    $course2 = trim($_POST['course2']);
    $course3 = trim($_POST['course3']);
    $course4 = trim($_POST['course4']);
    $course5 = trim($_POST['course5']);


    $applicant = new gradStudent(
      $firstname,
      $lastname,
      $mi,
      $phone1 . "-" . $phone2 . "-" . $phone3,
      $email,
      $directoryid,
      $uid,
      $gpa,
      $semesters,
      $department,
      $transcript,
      $prevta,
      $course1,
      $course2,
      $course3,
      $course4,
      $course5,
      $comments,
      $gradstatus,
      $masters,
      $semester,
      $year,
      $advisor,
      $currta,
      $step,
      $course,
      $instructor,
      $course1TAed,
      $course1TAedOverlap,
      $course1TAedGrade,
      $course1TAedTook,
      $course1TAedInstructor,
      $course2TAed,
      $course2TAedOverlap,
      $course2TAedGrade,
      $course2TAedTook,
      $course2TAedInstructor,
      $course3TAed,
      $course3TAedOverlap,
      $course3TAedGrade,
      $course3TAedTook,
      $course3TAedInstructor,
      $mei,
      $umei,
      $position,
      $preferredsemester,
      $preferredyear
  );

    $_SESSION['applicant'] = $applicant;
    $_SESSION['Student_type'] = "Grad";

    header("Location: reviewPage.php");
}
?>

<!DOCTYPE html>
<html>

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="Assets/Bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="Assets/Bootstrap/bootstrap.min.js">

  <title>Grad TA App</title>
  <style>
    @media (max-width: 623px) {
      h3 {
        font-size: 100%;
      }

      .page-heading {
        font-size: 150%;
      }
    }
  </style>
</head>

<body>

  <script src="https://umd-header.umd.edu/build/bundle.js?search=0&amp;search_domain=&amp;events=0&amp;news=0&amp;schools=0&amp;admissions=0&amp;support=0&amp;support_url=&amp;wrapper=0&amp;sticky=0"></script>
  <noscript>
    <br />
    <div class="um-brand pb-0 mb-0">
      <div id="logo">
        <a href="cs.umd.edu" target="_blank">University of Maryland</a>
      </div>
    </div>
  </noscript>


  <nav class="navbar navbar-dark shadow-sm p-3 mb-5 rounded" style="background-color: #e8e8e8">
    <img class="navbar-brand img-fluid col-5" src="Assets/logo.png">
  </nav>


  <h1 class="container text-center display-4 page-heading">Graduate Application</h1>

  <div class="container">

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">

      <fieldset class="form-group">

        <div class="card">

          <div class="card-header">
            Student Information
          </div>

          <div class="card-body">

            <div class="row">

              <div class="col-5">
                <label for="firstname">First Name</label>
                <input class="form-control" type="text" name="firstname" required="true">
              </div>

              <div class="col-2">
                <label for="mi">M.I.</label>
                <input class="form-control" type="text" name="mi">
              </div>

              <div class="col-5">
                <label for="lastname">Last Name</label>
                <input class="form-control" type="text" name="lastname" required="true">
              </div>

            </div>

            <div class="row">

              <div class="col-lg-6">
                <label for="phone">Phone</label>
                <div class="row mx-1">
                  <input class="form-control col-2" type="tel" pattern="\d{3}" placeholder="###" name="phone1" maxlength="3" required="true">-
                  <input class="form-control col-2" type="tel" pattern="\d{3}" placeholder="###" name="phone2" maxlength="3" required="true">-
                  <input class="form-control col-2" type="tel" pattern="\d{4}" placeholder="####" name="phone3" maxlength="4" required="true">
                </div>
              </div>

              <div class="col-lg-6">
                <label for="email">Email</label>
                <input class="form-control" type="email" name="email" required="true">
              </div>

            </div>

            <div class="row">

              <div class="col-4">
                <label for="uid">UID #</label>
                <input class="form-control" type="text" pattern="[0-9]{9}" placeholder="#########" maxlength="9" name="uid" required="true">
              </div>

              <div class="col-4">
                <label for="gpa">Cumulative GPA</label>
                <input class="form-control" type="text" pattern="[0-4][\.][0-9]{2}" maxlength="4" placeholder="#.##" name="gpa" required="true">
              </div>

              <div class="col-4">
                <label for="numSemesters">Completed Semesters at UMD</label>
                <input class="form-control" type="text" name="numSemesters" placeholder="#" required="true">
              </div>

            </div>

            <div class="row mt-3">

              <div class="col">
                <label>Department</label>
                <select class="form-control d-block w-50" name="department">
                  <option value="CS">Computer Science</option>
                  <option value="CE">Computer Engineering</option>
                  <option value="EE">Electrical Engineering</option>
                  <option value="OTHER">Other</option>
                </select>
                <label for="otherDepartment">Please tell the Department if you have selected "Other" above:</label>
                <input type="text" name="otherDepartment">
              </div>

              <div class="col">
                <label class="d-block">
                  Upload Unofficial Transcript (.pdf only)
                  <input class="d-block" name="transcript" type="file" accept=".pdf">
                </label>
              </div>
            </div>

            <div class="row">
              <div class="form-group col-5">
                <label for="prevta" class="d-block">I am a</label>
                <input type="radio" name="gradstatus" value="PHD" required="true">PhD Student
                <input type="radio" name="gradstatus" value="MASTERS">Masters Student
              </div>

              <div class="form-group col-5">
                <label for="prevta" class="d-block">I have my Masters degree</label>
                <input type="radio" name="masters" value="YES" required="true">Yes
                <input type="radio" name="masters" value="NO">No
              </div>
            </div>

            <div class="row">
              <div class="form-group col-5">
                <label for="entry" class="d-block">Entry Semester in Program</label>

                <select class="form-control w-50" name="semester" required="true">
                  <option value="fall">Fall</option>
                  <option value="spring">Spring</option>
                  <option value="Summer">Summer</option>
                  <option value="Winter">Winter</option>
                </select>


                <select class="form-control w-50" name="year" required="true">
                  <?php
                  for ($i = date("Y") - 6; $i <= date("Y") + 10; $i++) {
                      echo '<option value="' . $i . '">' . $i . '</option>';
                  }
                  ?>
                </select>

              </div>

              <div class="form-group col-5">
                <label class="d-block">Advisor</label>
                <input type="text" name="advisor" required="true">
              </div>
            </div>

          </div>

        </div>

        <div class="card">

          <div class="card-header">
            TA Information
          </div>

          <div class="card-body">

            <div class="form-group">
              <label for="prevta" class="d-block">I am currently a TA</label>
              <input type="radio" name="currta" value="YES" required="true">Yes
              <input type="radio" name="currta" value="NO">No
            </div>

            <div class="form-group">
              <label>If YES:</label>
              <div class="card">

                <div class="card-header">
                  Current Step:
                </div>

                <div class="card-body">
                  <select class="form-control" name="step">
                    <option value="I">Step I (first-year graduate assistants only)</option>
                    <option value="II">Step II (second-year assistants, or students holding an MS degree)</option>
                    <option value="III">Step III (Ph.D. students officially advanced to candidacy)</option>
                  </select>
                </div>

              </div>

              <div class="card">

                <div class="card-header">
                  Current Course:
                </div>

                <div class="card-body">
                  <select class="form-control" name="course">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

              </div>

              <div class="card">

                <div class="card-header">
                  Instructor:
                </div>

                <div class="card-body">
                  <input type="text" name="instructor">
                </div>

              </div>

            </div>

            <div class="form-group">
              <label for="prevta" class="d-block">I have been a CMSC TA before</label>
              <input type="radio" name="prevta" value="YES" required="true">Yes
              <input type="radio" name="prevta" value="NO">No
            </div>

            <br>

            <div class="form-group">
              <label class="d-block text-center">If yes, I have TAed for the following classes (top 3 most recent, if applicable):</label>
              <div class="row">
                <div class="col-md-2"><br>
                  <label>1st Class</label><br>
                  <select class="form-control" name="Course1TAed">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>


                <div class="col-md-4">
                  <label>Have you taken a course with this content, or 50% overlapping?</label><br>
                  <select class="form-control" name="Course1TAedOverlap">
                    <option value="YES" selected>Yes</option>
                    <option value="NO">No</option>
                  </select>
                </div>

                <div class="col-md-1"><br>
                  <label>Grade</label><br>
                  <select class="form-control" name="Course1TAedGrade">
                    <option value="--">--</option>
                    <option value="A+">A+</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="F">F</option>
                  </select>
                </div>



                <div class="col-md-2"><br>
                  <label>When you TOOK it:</label><br>
                  <select class="form-control" name="Course1TAedTook">
                    <option value="--">--</option>
                    <?php
                    $semNames = array("Fall", "Summer", "Spring");
                    for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                        for ($x = 0; $x < count($semNames); $x++) {
                            echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                        }
                        echo "<br>";
                    }
                    ?>
                  </select>
                </div>

                <div class="col-md-3"><br>
                  <label>Instructor</label>
                  <input class="form-control" type="text" name="Course1TAedInstructor">
                </div>

              </div>

            </div>

            <div class="form-group">

              <div class="row">
                <div class="col"><br>
                  <label>2nd Class</label><br>
                  <select class="form-control" name="Course2TAed">
                    <option value="" selected>--</option>

                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

                <div class="col-md-4">
                  <label>Have you taken a course with this content, or 50% overlapping?</label><br>
                  <select class="form-control" name="Course2TAedOverlap">
                    <option value="YES" selected>Yes</option>
                    <option value="NO">No</option>
                  </select>
                </div>

                <div class="col-md-1"><br>
                  <label>Grade</label><br>
                  <select class="form-control" name="Course2TAedGrade">
                    <option value="--">--</option>
                    <option value="A+">A+</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="F">F</option>
                  </select>
                </div>



                <div class="col-md-2"><br>
                  <label>When you TOOK it:</label><br>
                  <select class="form-control" name="Course2TAedTook">
                  <option value="--">--</option>
                    <?php
                    $semNames = array("Fall", "Summer", "Spring");
                    for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                        for ($x = 0; $x < count($semNames); $x++) {
                            echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                        }
                        echo "<br>";
                    }
                    ?>
                  </select>
                </div>

                <div class="col-md-3"><br>
                  <label>Instructor</label>
                  <input class="form-control" type="text" name="Course2TAedInstructor">
                </div>

              </div>

            </div>

            <div class="form-group">

              <div class="row">
                <div class="col"><br>
                  <label>3rd Class</label><br>
                  <select class="form-control" name="Course3TAed">
                    <option value="" selected>--</option>

                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

                <div class="col-md-4">
                  <label>Have you taken a course with this content, or 50% overlapping?</label><br>
                  <select class="form-control" name="Course3TAedOverlap">
                    <option value="YES" selected>Yes</option>
                    <option value="NO">No</option>
                  </select>
                </div>

                <div class="col-md-1"><br>
                  <label>Grade</label><br>
                  <select class="form-control" name="Course3TAedGrade">
                    <option value="--">--</option>
                    <option value="A+">A+</option>
                    <option value="A">A</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B">B</option>
                    <option value="B-">B-</option>
                    <option value="C+">C+</option>
                    <option value="C">C</option>
                    <option value="C-">C-</option>
                    <option value="D+">D+</option>
                    <option value="D">D</option>
                    <option value="D-">D-</option>
                    <option value="F">F</option>
                  </select>
                </div>



                <div class="col-md-2"><br>
                  <label>When you TOOK it:</label><br>
                  <select class="form-control" name="Course3TAedTook">
                  <option value="--">--</option>
                    <?php
                    $semNames = array("Fall", "Summer", "Spring");
                    for ($year = date("Y"); $year >= date("Y") - 6; $year--) {
                        for ($x = 0; $x < count($semNames); $x++) {
                            echo '<option value="' . strtoupper($semNames[$x] . $year) . '">' . $semNames[$x] . " " . $year . '</option>';
                        }
                        echo "<br>";
                    }
                    ?>
                  </select>
                </div>

                <div class="col-md-3"><br>
                  <label>Instructor</label>
                  <input class="form-control" type="text" name="Course3TAedInstructor">
                </div>

              </div>
            </div>

            <div class="form-group">
              <label for="priorTAexp" class="control-label">Prior TA experience when not at UMD</label>
              <div class="col-sm-9">
                <textarea rows="5" class="form-control" name="priorTAexp"></textarea>
              </div>
            </div>

            <br>

            <div class="form-group">
              <label class="d-block">List the top 5 courses you would like to TA for</label>
              <div class="row">
                <div class="col">
                  <label>1st Choice</label>
                  <select class="form-control" name="course1" required="true">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

                <div class="col">
                  <label>2nd Choice</label>
                  <select class="form-control" name="course2" required="true">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

                <div class="col">
                  <label>3rd Choice</label>
                  <select class="form-control" name="course3" required="true">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

                <div class="col">
                  <label>4th Choice</label>
                  <select class="form-control" name="course4">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>

                <div class="col">
                  <label>5th Choice</label>
                  <select class="form-control" name="course5">
                    <option value="" selected>--</option>
                    <?php
                    foreach ($classes as $class) {
                        echo '<option value="' . $class . '">' . $class . '</option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">

              <div class="row">

                <div class="col-4">
                  <label class="d-block">Position Type</label>
                  <input type="radio" name="position" value="FULL_TIME" required="true"> Full-time 20hrs/week
                  <input type="radio" name="position" value="PART_TIME" required="true"> Part-time 10hrs/week
                </div>

                <div class="col-4">
                  <label class="d-block">Semester</label>
                  <select class="form-control" name="preferredsemester" required="true">
                    <option value="fall"> Fall</option>
                    <option value="spring">Spring</option>
                    <option value="summer">Summer</option>
                  </select>
                </div>

                <div class="col-4">
                  <label class="d-block">Year</label>
                  <select class="form-control" name="preferredyear" required="true">
                    <?php
                    for ($year = date("Y") - 1; $year <= date("Y") + 6; $year++) {
                        echo '<option value="' . $year . '" required="true">' . $year . '</option>';
                    }
                    ?>
                  </select>
                </div>

              </div>

            </div>

            <div class="form-group">
              <div class="card">
                <div class="card-header">
                  For International Students
                </div>

                <div class="card-body">
                  <div class="row">
                    <div class="col-6">
                      <label class="d-block">Have you passed the Maryland English Institute (MEI Evaluation)?</label>
                      <input type="radio" name="mei" value="YES">Yes
                      <input type="radio" name="mei" value="NO">No
                    </div>

                    <div class="col-6">
                      <label class="d-block">Are you currently taking a UMEI course?</label>
                      <input type="radio" name="umei" value="YES">Yes
                      <input type="radio" name="umei" value="NO">No
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="card">
                <div class="card-header">
                  Additional Comments
                </div>
                <label for="comments" class="control-label"></label>
                <div class="col-sm-9">
                  <textarea rows="5" class="form-control" name="comments"></textarea>
                </div>
              </div>
            </div>

          </div>
        </div>
  </div>

  <div class="form-group">

    <blockquote class="blockquote text-center">
      <p class="text-center"><strong>By checking the box below, you agree that you: </strong></p>
      <p>1. Are eligible to TA for the classes you have selected.</p>
      <p>2. Understand that you may not hear back about a TA position by the end of the semester.</p>
      <p>3. Will not e-mail the faculty member/instructor directly regarding TA positions.</p>
    </blockquote>

    <div class="form-check form-control-lg text-center">
      <label class="form-check-label">
        <input type="checkbox" name="waiver" required="true"> <mark>I agree.</mark>
      </label>
    </div>

  </div>

  <div class="form-group text-center">
    <button class="btn btn-lg btn-primary" type="submit" name="submitApp">Submit Application</button>
  </div>

  </fieldset>

  </form>

  </div>
  <footer class="page-footer font-small mt-4 pt-2" style="background-color: #e8e8e8">

    <div class="container-fluid text-center">
      <a href="https://cs.umd.edu">UMD CS</a>
    </div>

    <div class="footer-copyright text-center py-3">@ <?php echo date("Y"); ?> Copyright:
      <a href="https://www.umd.edu">University of Maryland</a>
    </div>

  </footer>

</body>

</html>