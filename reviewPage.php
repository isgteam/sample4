<?php
/* This page is rendered when a grad or ugrad has finished completing their application and this page shows them some of their information before finally feeding in the DB. 

Maybe future improvements:
1) Show more appropriate summary of the data
*/
require_once($_SERVER['DOCUMENT_ROOT'].'/Student/Student.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/Student/gradStudent.php');
require_once('support.php');
session_start();

$applicant = $_SESSION['applicant'];

$backPage = "gPage.php";

if (strcmp($_SESSION['Student_type'],"Ugrad") ==  0){
	$backPage = "ugPage.php";
}

$body = '
		<p class="text-center">Name: '.$applicant->getFirstName().' '.$applicant->getLastName().'
		</p>

		<p class="text-center">UID: '.$applicant->getUID().'
		</p>

		<p class="text-center">Phone Number: '.$applicant->getPhone().'
		</p>

		<p class="text-center">Email: '.$applicant->getEmail().'
		</p>

		<p class="text-center">Choices: '.$applicant->getCourse1().', '.$applicant->getCourse2().', 
'.$applicant->getCourse3().', '.$applicant->getCourse4().', '.$applicant->getCourse5().'
		</p>

		<p class="text-center">Comments: '.$applicant->getComments().'
		</p>

		<form action="'.$_SERVER['PHP_SELF'].'" method="post">
			<div class="form-group text-center">
				<input type="button" onclick="window.history.back()" class="btn btn-lg btn-secondary" name="changeApp" id="changeApp" value="No, Change Information">
			</div> 

			<div class="form-group text-center">
				<a href="confirmationPage.php" class="btn btn-lg btn-primary" role="button" name="submitAppFinal" id="submitAppFinal">Yes, Submit Application</a>
			</div>
		</form>
';

generatePage($body,"Review Your Information","Is everything correct?");

?>