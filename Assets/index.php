<?php
/* This is a page only designed so that this route is not open.

 */
require '../vendor/autoload.php';

// Enable debugging
phpCAS::setDebug();
// Enable verbose error messages. Disable in production!
phpCAS::setVerbose(true);

// Initialize phpCAS
phpCAS::client(CAS_VERSION_2_0, 'login.umd.edu', 443, "cas");

// For production use set the CA certificate that is the issuer of the cert
// on the CAS server and uncomment the line below
// phpCAS::setCasServerCACert($cas_server_ca_cert_path);

// For quick testing you can disable SSL validation of the CAS server.
// THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
// VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
phpCAS::setNoCasServerValidation();

// force CAS authentication
phpCAS::forceAuthentication();

if (strcmp(phpCAS::getUser(),"apoorv") != 0 && strcmp(phpCAS::getUser(),"thurst") != 0){
	header("Location: /index.php");
}

header("Location: /Admin/index.php")
?>
